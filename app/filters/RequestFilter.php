<?php

namespace Filters;


class RequestFilter
{
    static public function validateJsonFormat($request_json_arg, $pcre_format = null)
    {
        if (isset($request_json_arg)) {
            $json = json_decode(html_entity_decode($request_json_arg));
            if (json_last_error() == JSON_ERROR_NONE) {
                if (is_null($pcre_format) || (preg_match_all($pcre_format, $request_json_arg) != 0)) {
                    return $json;
                }
            }
        }
        return false;
    }
} 