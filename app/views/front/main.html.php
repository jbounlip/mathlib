<div id="wrapper" class="col-md-8 col-md-offset-2">
    <h1>Diagonalisation</h1>

    <ul class="nav nav-tabs">
      <li role="presentation" onclick="changePhase(this, 1)" class="active"><a href="javascript:void(0);">Phase 1</a></li>
      <li role="presentation" onclick="changePhase(this, 2)"><a href="javascript:void(0);">Phase 2</a></li>
      <li role="presentation" onclick="changePhase(this, 3)"><a href="javascript:void(0);">Phase 3</a></li>
    </ul>
    <div id="content" class="col-md-12">
      <form id="form-math">
        <input name="step" id="input-step" type="hidden" value="1">
        <div class="col-md-12">
          <div id="div-poly" class="col-md-6">
            <h3>Calcul polynômial</h3>
            <h4>racines entières d'un polyôme de degrée 3</h4>
            <br>
                <div id="main-poly" class="form-inline">
<!--                    <label for="input-poly-coeff-3">P(X) =</label>-->
<!--                    <div class="input-group" >-->
<!--                        <input name="poly-coeff-3" id="input-poly-coeff-3" type="text" value="" placeholder="e.g. -1" class="form-control" pattern="^-?\d+$" required>-->
<!--                        <span class="input-group-addon">X<sup>3</sup></span>-->
<!--                    </div>-->
<!--                    <label for="input-poly-coeff-2">+</label>-->
<!--                    <div class="input-group ">-->
<!--                        <input name="poly-coeff-2" id="input-poly-coeff-2" type="text" value="0" placeholder="0" class="form-control" pattern="^-?\d+$" required>-->
<!--                        <span class="input-group-addon">X<sup>2</sup></span>-->
<!--                    </div>-->
<!--                    <label for="input-poly-coeff-1">+</label>-->
<!--                    <div class="input-group ">-->
<!--                        <input name="poly-coeff-1" id="input-poly-coeff-1" type="text" value="12" placeholder="12" class="form-control" pattern="^-?\d+$" required>-->
<!--                        <span class="input-group-addon">X</span>-->
<!--                    </div>-->
<!--                    <label for="input-poly-coeff-0">+</label>-->
<!--                    <div class="input-group ">-->
<!--                        <input name="poly-coeff-0" id="input-poly-coeff-0" type="text" value="-16" placeholder="-16" class="form-control" pattern="^-?\d+$" required>-->
<!--                    </div>-->
                </div>
              <br>
          </div>
          <div id="div-matrix" class="col-md-6 hidden">
            <h3>Calcul matriciel</h3>
            <h4>polynôme caractéristique et valeurs propres d'une matrice carrée de degrée 3</h4>
            <br>
            <table>
              <tr>
                <th class="matrix-title">A = </th>
                <td>
                  <table id="main-matrix" class="matrix">
                  </table>
                </td>
              </tr>
            </table>
            <br>
          </div>
          <div class="col-md-6">
            <fieldset class="fieldset-options">
                <legend>Options</legend>
                <label for="select-degree">n =</label>
                <select id="select-degree" onchange="changeDegree(this.value)">
                    <option value="2">2</option>
                        <option value="3" selected>3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                </select>
                <br>
                <label for="select-input-type">Domaine d'entrée:</label>
                <select id="select-input-type" onchange="changeInputType(this.value)">
                    <option value="integer" selected>N</option>
                    <option value="float">R</option>
                </select>
                <br>
                <label for="select-output-type">Domaine de sortie:</label>
                <select id="select-output-type" onchange="changeOutputType(this.value)">
                    <option value="integer" selected>N</option>
                    <option value="float">R</option>
                    <option value="complex">C</option>
                </select>
                <br>
                <div id="only-output-integer">
                    <label for="input-integer-try-from">Candidat minimum:</label>
                    <input id="input-integer-try-from" style="width:50px;" type="number" value="-10" pattern="^-?\d+$" required>
                    <label for="input-integer-try-from">maximum:</label>
                    <input id="input-integer-try-to" style="width:50px;" type="number" value="10" pattern="^-?\d+$" required>
                </div>
                <a class="btn btn-default" onclick="setRandomInputValuesAndPlaceholder()">Remplir aléatoirement</a>
                <a class="btn btn-default" onclick="emptyInputValuesAndPlaceholder()">Vider</a>
            </fieldset>
            <button type="submit" class="btn btn-default">Calculer</button>
          </div>
        </div>
      </form>
      <div id="div-result" class="col-md-12 hidden">
        <h3>Résultat</h3>
        <div id="p-result"></div>
        <div id="p-result2"></div>
      </div>
    </div>
</div>
