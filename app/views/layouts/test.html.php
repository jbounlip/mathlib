<?php

//$p4_ = array(-236 * )

$p = new \Math\Polynomial(array(-16, 12, 0, -1), false, 'integer', 'integer');
echo "<pre>p: $p</pre>";

echo "<pre>comp:";
$m = $p->companionMatrix();
echo($m);
echo "</pre>";

echo "<pre>p-roots-mul (by m):";
$resolver = new \Math\Calculator\MatrixResolverQR($m);

$resolver->calculate();

$ret = $resolver->getEigenvalues();

var_dump($ret);
//var_dump($p->getRoots());
//var_dump($p->getMultiplicities());
echo "</pre>";

$A = new Math\Matrix([
    [8, -10, 2, 45, 100],
    [-9, 6, 5, 5, 100],
    [-3, 8, 2, 10, 100],
    [-3, 15, -6, 10, 100],
    [1, 2, 3, 4, 5],
]);

echo "<pre>a=$A</pre>";

$resolver = new \Math\Calculator\MatrixResolverQR($A);

$resolver->calculate();

$ret = $resolver->getEigenvalues();

echo "<pre>";
var_dump($ret);
echo "</pre>";

$vp = $A->eigenvalues();

echo "<pre>";
var_dump($vp);
echo "</pre>";

$poly = $A->characteristicPolynomial();

echo "<pre>";
var_dump($poly);
echo "</pre>";




//$m = $p->toCompanionMatrix();
//
////debug($m);
//
//$cp = $m->characteristicPolynomial();
//
//debug($cp);
//
//$toto = $cp->recalculateIntegerRootsAndMultiplicities();
//debug($toto);
//
//$resolver = new \Math\Calculator\MatrixResolverQR($m);
//$resolver->calculate();
//
//debug($resolver->isCalculated());
//debug($resolver->getEigenvalues());

//$a = new \Math\Matrix(array(
////    [8, -10, 2, 45, 100],
////    [-9, 6, 5, 5, 100],
////    [-3, 8, 2, 10, 100],
////    [-3, 15, -6, 10, 100],
////    [1, 2, 3, 4, 5],
//
////    [8.000, -10.000,   2.000 , 45.000],
////[-9.000,   6.000 ,  5.000 ,  5.000],
////[-3.000 ,  8.000 , -2.000 , 10.000],
////[-3.000  , 8.000 , -4.000 ,  9.000],
//
//    [8, -10, 2, 4],
//    [-9, 6, 5, 5],
//[1, 2, -3, 10],
//[4, 5, -6, 8],
//
//));
//$decomposer = new \Math\MatrixDecompositionQRHouseholder($a);
//$resolver = new \Math\MatrixResolverQR($a, 0.000000001, 501);
//
//echo "<br>A=<pre>"; var_dump($a->toArray()); echo "</pre>";
//
//$resolver->resolve();
//
//$decomposer->process();
//
////echo "<br>Q=<pre>"; var_dump($decomposer->getQ()->toArray()); echo "</pre>";
////
////echo "<br>R=<pre>"; var_dump($decomposer->getR()->toArray()); echo "</pre>";
//
//echo "<br>vp=<pre>"; var_dump($resolver->getEigenvalues()); echo "</pre>";
