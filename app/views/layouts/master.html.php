<!DOCTYPE html>
<html>
<head lang="fr">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="/">
    <title>Diagonalisation project</title>
    <!-- Bootstrap -->
    <link href="<?= ASSET_BASE_URL ?>/css/bootstrap.min.css?version=3.3.1" rel="stylesheet">
    <link href="<?= ASSET_BASE_URL ?>/css/main.css" rel="stylesheet" type="text/css"/>

    <!-- MathJax -->
    <script src="<?= ASSET_BASE_URL ?>/js/ASCIIMathML.js?version=2.1"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="<?= ASSET_BASE_URL ?>/js/html5shiv.min.js?version=3.7.2"></script>
    <script src="<?= ASSET_BASE_URL ?>/js/respond.min.js?version=1.4.2"></script>
    <![endif]-->
</head>
<body>

    <?= $content ?>

    <script src="<?= ASSET_BASE_URL ?>/js/jquery.min.js?version=1.11.1"></script>
    <script src="<?= ASSET_BASE_URL ?>/js/bootstrap.min.js?version=3.3.1"></script>
    <script src="<?= ASSET_BASE_URL ?>/js/main.js" type="text/javascript"></script>
</body>
</html>
