<?php

if (isset($_SERVER['REQUEST_URI'])) {
    if (!empty($controller) && !empty($action)) {
        $controller = ucfirst($controller);
        $controller_class = 'Controllers\\' . $controller . 'Controller';
        if (class_exists($controller_class) && array_key_exists('Controllers\\BaseController', class_parents($controller_class))) {
            $ctrl = $controller_class::getInstance();
            if (call_user_func_array(array($ctrl, 'call'), array($action, $_SERVER['REQUEST_METHOD'], $_REQUEST))) {
                return ;
            }
        }
    } elseif (preg_match_all('#^(?:/?([a-z-_]+)(?:/([a-z-_]+))?)?#i', $_SERVER['REQUEST_URI'], $matches) != false) {
        $route_arg_one = $matches[1][0];
        $route_arg_two = $matches[2][0];

        $controller = ucfirst($route_arg_one);
        $action = $route_arg_two;
        if (empty($controller)) {
            $controller = 'Front';
        }
        if (empty($action)) {
            $action = 'Index';
        }

        $controller_class = 'Controllers\\' . $controller . 'Controller';
        if (class_exists($controller_class) && array_key_exists('Controllers\\BaseController', class_parents($controller_class))) {
            $ctrl = $controller_class::getInstance();
            if (call_user_func_array(array($ctrl, 'call'), array($action, $_SERVER['REQUEST_METHOD'], $_REQUEST))) {
                return ;
            }
        }
    }
}

header("HTTP/1.0 501 Not Implemented");
if (DEBUG_FLAG) {
    $error_msg = "erreur 501 (route " . $_SERVER['REQUEST_URI'] . " does not exist).";
    $error_msg .= " Controller: " . (empty($controller) ? "none" : $controller) . ".";
    $error_msg .= " Action: " . (empty($action) ? "none" : $action) . ".";
    throw new \Exception($error_msg);
}
