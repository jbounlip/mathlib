<?php

namespace Math;


interface IFunction
{
    /*-------------------------------------`
     * Accessors / Mutators                *
     `-------------------------------------*/

    /** @return string */
    public function getInputType();

    /** @return string */
    public function getOutputType();

    /** @return boolean */
    public function isInputType($input_type);

    /** @return boolean */
    public function isOutputType($output_type);

    /*-------------------------------------`
     * Specifics methods                   *
     `-------------------------------------*/
    /** @return boolean */
    public function verifyInputType($var);

    /** @return boolean */
    public function verifyOutputType($var);

    /** @return integer|float|double|\Math\Complex */
    public function calculateForValue($x);
}
