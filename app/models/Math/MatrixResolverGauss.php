<?php
//
// MatrixResolverGauss.php for  in /Users/jb/proj/etna/mat2/bounli_j
// 
// Made by BOUNLIPHONE Jacques
// Login   <bounli_j@etna-alternance.net>
// 
// Started on  Thu Jan  9 11:25:03 2014 BOUNLIPHONE Jacques
// Last update Thu Jan 23 06:08:02 2014 BOUNLIPHONE Jacques
//
namespace Math;

error_reporting(E_ALL);

//require_once("inc/Matrix.php");
//require_once("inc/MatrixTools.php");

class MatrixResolverGauss
{
  const STANDARD_GAUSS = FALSE;
  const INTER_GAUSS_TO_GENERATOR = TRUE;

  /**
   * VERSION PHP < 5.5 sans yield
   * @return:	tableau de tableaux contenant les G_i A_i Y_i
   **/
  public static function	resolve5(Matrix $A, Matrix $Y)
  {
    if (!$A->isCarre() || $Y->getNbrCol() !== 1 || $Y->getNbrLin() !== $A->getNbrLin())
      {
	echo "ERROR: Can't be resolved.\n";
	return (FALSE);
      }
    return self::get_results_table($A, $Y);
  }

  private static function	get_results_table($A, $Y)
  {
    $res = array();
    for ($step = 0; $step < $A->getNbrLin() - 1; ++$step)
      {
  	$res[] = ($new_gayi = self::inter_values_gauss($A, $Y, $step));
  	$A = $new_gayi['A'];
  	$Y = $new_gayi['Y'];
      }
    $res['xyz'] = self::get_results_xyz($A, $Y);
    return $res;
  }

  private static function	get_results_xyz(Matrix $An, Matrix $Yn)
  {
    $n = $An->getNbrLin();

    $res[$n - 1] = $Yn($n - 1, 0) / $An($n - 1, $n - 1);
    for ($i = $n - 2; $i >= 0; --$i)
      {
	$zeta = 0;
	for ($j = $n - 1; $j > $i; --$j)
	    $zeta += $res[$j] * $An($i, $j);
	$zeta -= $Yn($i, 0);
	$res[$i] = $zeta / -$An($i, $i);
      }
    return ($res);
  }
  /* ------------- FIN resolve method  php < 5.5 ------------- */

  /**
   * VERSION PHP 5.5 avec YIELD
   * @return:	tableau contenant G_n A_n Y_n
   *		ou iterateur contenant toutes les valeurs intermediaires de G_i A_i Y_i et i
   **/
  public static function	resolve(Matrix $A, Matrix $Y, $get_inter = self::STANDARD_GAUSS)
  {
    if (!$A->isCarre() || $Y->getNbrCol() !== 1 || $Y->getNbrLin() !== $A->getNbrLin())
      {
	echo "ERROR: Can't be resolved.\n";
	return (FALSE);
      }
    $gen = self::gayi_gen($A, $Y);
    foreach ($gen as $gayi)
      {
    	if ($get_inter)
    	  return $gen;
      }
    return ($gayi);
  }

  private static function	gayi_gen($A, $Y)
  {
    for ($step = 0; $step < $A->getNbrLin() - 1; ++$step)
      {
  	yield ($new_gayi = self::inter_values_gauss($A, $Y, $step));
  	$A = $new_gayi['A'];
  	$Y = $new_gayi['Y'];
      }
  }
  /* ------------- FIN resolve method php 5.5 ------------- */


  private static function	inter_values_gauss($A, $Y, $step)
  {
    self::swap_lines($A, $Y, $step);
    $g = array(array());
    for ($i = 0; $i < $A->getNbrLin(); ++$i)
      for ($j = 0; $j < $A->getNbrLin(); ++$j)
	$g[$i][$j] = ($i === $j) ? 1 : 0;
    for ($i = $step + 1; $i < $A->getNbrLin(); ++$i)
      $g[$i][$step] = -$A($i, $step) / $A($step, $step); // div classique
    $G = new Matrix($g);
    return (array('G' => $G, 'A' => MatrixTools::prod($G, $A), 'Y' => MatrixTools::prod($G, $Y), 'i' => $step));
  }

  private static function	swap_lines(&$A, &$Y, $step)
  {
    $line_to_swap = $step;
    for ($i = 0; $i < $A->getNbrLin(); ++$i)
      {
	if ($A($line_to_swap, $step) === 0) // juste pour le 0
	  ++$line_to_swap;
	else
	  break;
      }
    if ($line_to_swap === $step)
      return TRUE;
    if ($line_to_swap >= $A->getNbrLin())
      {
	echo "Error: Can't be resolved\n";
	return FALSE;
      }
    $A->swapLines($step, $line_to_swap);
    $Y->swapLines($step, $line_to_swap);
  }
}
