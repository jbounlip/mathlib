<?php

namespace Math;

class Polynomial implements IFunction
{
    protected $input_type = 'float';
    protected $output_type = 'float';
    protected $degree = -1;
    protected $coefficients = array();
    protected $roots = array();
    protected $multiplicities = array();
    protected $scalar_factor = null;
    protected $extra_params = array(
        'integer_roots_try_from' => -10,
        'integer_roots_try_to' => 10,
        'float_qr_epsilon' => 0.000001,
        'float_qr_max_iteration' => 100,
        'float_precision' => 2,
    );

    /*-------------------------------------`
     * Static methods                      *
     `-------------------------------------*/

    public static function add(Polynomial $p, Polynomial $q, $input_type = 'float', $output_type = 'float')
    {

        if ($p->equalsZero() && $q->equalsZero()) {
            return new Polynomial([0]);
        }
        if ($p->equalsZero() && $q->isValid()) {
            return $q;
        }
        if ($q->equalsZero() && $p->isValid()) {
            return $p;
        }
        if (!$p->isValid() || !$q->isValid()) {
            return null;
        }

        if ($p->getDegree() > $q->getDegree()) {
            $bigger = $p;
            $smaller = $q;
        } else {
            $bigger = $q;
            $smaller = $p;
        }
        $coefficients_arr = array();
        $i = 0;

        while ($i <= $smaller->getDegree()) {
            $coefficients_arr[$i] = $bigger->getCoefficient($i) + $smaller->getCoefficient($i);
            ++$i;
        }
        while ($i <= $bigger->getDegree()) {
            $coefficients_arr[$i] = $bigger->getCoefficient($i);
            ++$i;
        }
        return new Polynomial($coefficients_arr, false, $input_type, $output_type);
    }

    public static function subtract(Polynomial $p, Polynomial $q, $input_type = 'float', $output_type = 'float')
    {
        if (!$p->isValid() || !$q->isValid()) {
            return null;
        }
        return self::add($p, self::scalarMultiply($q, -1, $input_type, $output_type), $input_type, $output_type);
    }

    public static function multiply(Polynomial $p, Polynomial $q, $input_type = 'float', $output_type = 'float')
    {
        //@TODO
        throw new \Exception('NIY');
    }

    public static function euclideanDivide(Polynomial $a, Polynomial $b, $input_type = 'float', $output_type = 'float')
    {
        if (!$a->isValid() || !$b->isValid() || $a->getDegree() < $b->getDegree() || $b->toArray() == [0]) {
            return null;
        }

        $q = new Polynomial([0]);
        $r = $a;
        while (!$r->equalsZero() && $r->getDegree() >= $b->getDegree()) {
            $t = Monomial::divide(new Monomial($r->leadCoefficient(), $r->getDegree()), new Monomial($b->leadCoefficient(), $b->getDegree()));
            $q = self::add($q, $t);
            $r = self::subtract($r, $t->operatorPolynomialMultiply($b));
        }

        return array(
            'quotient' => $q,
            'remainder' => $r,
        );
    }

    public static function scalarMultiply(Polynomial $p, $k, $input_type = 'float', $output_type = 'float')
    {
        if (!$p->isValid() || !$p->verifyInputType($k)) {
            return null;
        }

        $coefficients_arr = array();
        $i = 0;

        while ($i <= $p->getDegree()) {
            $coefficients_arr[$i] = $k * $p->getCoefficient($i);
            ++$i;
        }
        return new Polynomial($coefficients_arr, false, $input_type, $output_type);
    }

    /*-------------------------------------`
     * Constructor && Magic methods        *
     `-------------------------------------*/

    public function __construct(array $data, $factorized_form_flag = false, $input_type = 'float', $output_type = 'float')
    {
        $this->input_type = $input_type;
        $this->output_type = $output_type;
        if ($factorized_form_flag) {
            if (isset($data['roots']) && isset($data['multiplicities']) && isset($data['scalar_factor'])
                && count($data['roots']) > 0
                && count($data['roots']) == count($data['multiplicities'])
                && $this->verifyInputType($data['scalar_factor'])
            ) {
                $this->roots = $data['roots'];
                $this->multiplicities = $data['multiplicities'];
                $this->scalar_factor = $data['scalar_factor'];
            }
        } else {
            for ($i = count($data) - 1; $i >= 0; --$i) {
                if ($data[$i] != 0) {
                    break;
                }
                unset($data[$i]);
            }
            if (empty($data)) {
                $data[0] = 0;
            }
            foreach ($data as $c) {
                if (!$this->verifyInputType($c)) {
                    return ;
                }
            }
            $this->coefficients = $data;
        }
    }

    public function __invoke($i)
    {
        return $this->getCoefficient($i);
    }

    public function __toString()
    {
        $str = "[";
        for ($i = 0; $i <= $this->getDegree(); ++$i) {
            $str .= $this->getCoefficient($i) . ", ";
        }
        $str = substr($str, 0, count($str) - 3);
        $str .= "]";
        return ($str);
    }

    /*-------------------------------------`
     * Accessors / Mutators                *
     `-------------------------------------*/

    public function getInputType()
    {
        return $this->input_type;
    }

    public function getOutputType()
    {
        return $this->output_type;
    }

    public function isInputType($input_type)
    {
        return $this->input_type == $input_type;
    }

    public function isOutputType($output_type)
    {
        return $this->output_type == $output_type;
    }

    public function getDegree()
    {
        if (!($this->degree >= 0)) {
            if (!empty($this->coefficients)) {
                $this->degree = count($this->coefficients) - 1;
            } else {
                $degree = 0;
                foreach ($this->multiplicities as $m) {
                    $degree += $m;
                }
                $this->degree = $degree;
            }
        }
        return $this->degree;
    }

    public function getCoefficients()
    {
        if (empty($this->coefficients)) {
            $this->expandAndSimplify();
        }
        return $this->coefficients;
    }

    public function getCoefficient($i)
    {
        if (empty($this->coefficients)) {
            $this->getCoefficients();
        }
        return isset($this->coefficients[$i]) ? $this->coefficients[$i] : null;
    }

    public function getRoots()
    {
        if (empty($this->roots)) {
            $this->factorize();
        }
        return $this->roots;
    }

    public function getRoot($i)
    {
        if (empty($this->roots)) {
            $this->getRoots();
        }
        return isset($this->roots[$i]) ? $this->roots[$i] : null;
    }

    public function getMultiplicities()
    {
        if (empty($this->multiplicities)) {
            $this->factorize();
        }
        return $this->multiplicities;
    }

    public function getMultiplicity($i)
    {
        if (empty($this->multiplicities)) {
            $this->getMultiplicities();
        }
        return isset($this->multiplicities[$i]) ? $this->multiplicities[$i] : null;
    }

    public function getScalarFactor()
    {
        if (is_null($this->scalar_factor) && !empty($this->roots)) {
            $this->scalar_factor = $this->leadCoefficient();
//            if ($this->leadCoefficient() < 0) {
//                $this->scalar_factor = $this->getDegree() % 2 == 0 ? $this->leadCoefficient() : -$this->leadCoefficient(); //to verify ?
//            } else {
//                $this->scalar_factor = $this->getDegree() % 2 == 0 ? -$this->leadCoefficient() : $this->leadCoefficient(); //to verify ?
//            }

        }
        return $this->scalar_factor;
    }

    public function toArray($factorized_form_flag = false)
    {
        if (!$factorized_form_flag) {
            return $this->getCoefficients();
        } else {
            return array(
                'roots' => $this->getRoots(),
                'multiplicities' => $this->getMultiplicities(),
                'scalar_factor' => $this->getScalarFactor(),
            );
        }
    }

    public function getExtraParams()
    {
        return $this->extra_params;
    }

    public function setExtraParams(array $extra_params)
    {
        $this->extra_params = $extra_params;
    }

    public function getExtraParam($key)
    {
        return isset($this->extra_params[$key]) ? $this->extra_params[$key] : null;
    }

    public function setExtraParam($key, $value)
    {
        $this->extra_params[$key] = $value;
    }

    /*-------------------------------------`
     * Specifics methods                   *
     `-------------------------------------*/

    public function verifyInputType($var)
    {
        return $this->verifyType($var, 'input_type');
    }

    public function verifyOutputType($var)
    {
        return $this->verifyType($var, 'output_type');
    }

    public function isValid()
    {
        return (count($this->coefficients) > 0) || (count($this->roots) > 0 && count($this->roots) == count($this->multiplicities)); //@TODO: verifier les types
    }

    public function equalsTo($polynomial_or_array, $factorized_form_flag = false, $epsilon = 0.0)
    {
        if (is_array($polynomial_or_array)) {
            $q = new Polynomial($polynomial_or_array, $factorized_form_flag);
        } else if ($polynomial_or_array instanceof Polynomial) {
            $q = &$polynomial_or_array;
        } else {
            return false;
        }
        for ($i = 0; $i <= $this->getDegree(); ++$i) {
            if (abs($this->getCoefficient($i) - $q->getCoefficient($i)) > $epsilon) {
                return false;
            }
        }
        return true;
    }

    public function equalsZero() {
        for ($i = 0; $i <= $this->getDegree(); ++$i) {
            if ($this->getCoefficient($i) != 0) {
                return false;
            }
        }
        return true;
    }

    public function leadCoefficient() {
        return $this->getCoefficient($this->getDegree());
    }

    public function calculateForValue($x)
    {
        if (!$this->isValid()) {
            return null;
        }

        $result = 0;
        $i = 0;
        while ($i <= $this->getDegree()) {
            $result += $this->getCoefficient($i) * pow($x, $i);
            ++$i;
        }
        return $result;
    }

    public function calculateIntegerRoots($try_from = -10, $try_to = 10)
    {
        if (!$this->isValid()) {
            return ;
        }
        $this->roots = array();
        $try_from = intval($try_from);
        $try_to = intval($try_to);

        for ($n = $try_from; $n <= $try_to; ++$n) {
            if ($this->calculateForValue($n) == 0) {
                if (!in_array($n, $this->roots)) {
                    $this->roots[] = $n;
                }
            }
        }
    }

    public function calculateMultiplicities()
    {
        if (!$this->isValid() || empty($this->roots)) {
            return ;
        }
        $this->multiplicities = array();
        $degree = $this->getDegree();
        $roots_count = count($this->getRoots());

        switch ($roots_count) {
            case 1:
                $this->multiplicities[0] = $degree;
                break;
            case $degree:
                for ($i = 0; $i < $degree; ++$i) {
                    $this->multiplicities[$i] = 1;
                }
                break;
            default:
                // deg = 3
                if ($degree == 3 && $roots_count == 2) {
                    $arr_one = function($n) {
                        $arr = array();
                        for ($i = 0; $i < $n; ++$i) {
                            $arr[] = 1;
                        }
                        return $arr;
                    };
                    $q = new Polynomial(array(
                        'roots' => $this->getRoots(),
                        'multiplicities' => $arr_one($roots_count),
                        'scalar_factor' => 1
                    ), true);
                    $res = Polynomial::euclideanDivide($this, $q);
                    $q_0 = $res['quotient'];
                    $exp_0 =  new Polynomial(array(
                        'roots' => array($this->getRoot(0)),
                        'multiplicities' => array(1),
                        'scalar_factor' => -1
                    ), true);
                    if ($q_0->equalsTo($exp_0)) {
                        $this->multiplicities[0] = 2;
                        $this->multiplicities[1] = 1;
                    } else {
                        $this->multiplicities[0] = 1;
                        $this->multiplicities[1] = 2;
                    }
                } else {

                }
                break;
        }
    }

    public function factorize(array $params = array())
    {
        if (!$this->isValid()) {
            return ;
        }
        switch ($this->output_type) {
            case 'integer':
                $try_from = intval($this->getExtraParam('integer_roots_try_from'));
                $try_to = intval($this->getExtraParam('integer_roots_try_to'));

                $this->calculateIntegerRoots($try_from, $try_to);
                $this->calculateMultiplicities();
                $this->getScalarFactor();
                break;
            case 'numeric':
            case 'double':
            case 'float':
                $qr_epsilon = floatval($this->getExtraParam('float_qr_epsilon'));
                $qr_max_iteration = intval($this->getExtraParam('float_qr_max_iteration'));
                $float_precision = intval($this->getExtraParam('float_precision'));
                $roots_precision = 1 / pow(10, $float_precision);

                $resolver = new \Math\Calculator\MatrixResolverQR($this->companionMatrix(), $qr_epsilon, $qr_max_iteration);
                $resolver->calculate();
                $ev = $resolver->getEigenvalues();

                $roots = array();
                $multiplicities = array();
                for ($i = 0; $i < count($ev); ++$i) {
                    if (!is_numeric($ev[$i])) {
                        $this->roots = array();
                        $this->multiplicities = array();
                        $this->scalar_factor = null;
                        return;
                    }
                    if (empty($roots)) {
                        $roots[] = round($ev[$i], $float_precision);
                        $multiplicities[] = 1;
                    } else {
                        if (abs($roots[$i - 1] - $ev[$i]) < $roots_precision) {
                            $multiplicities[$i - 1]++;
                        } else {
                            $roots[] = round($ev[$i], $float_precision);
                            $multiplicities[] = 1;
                        }
                    }
                }
                $this->roots = $roots;
                $this->multiplicities = $multiplicities;
                $this->getScalarFactor();
                break;
            case 'complex':
                //@TODO: refactor
                $qr_epsilon = floatval($this->getExtraParam('float_qr_epsilon'));
                $qr_max_iteration = intval($this->getExtraParam('float_qr_max_iteration'));
                $float_precision = intval($this->getExtraParam('float_precision'));
                $roots_precision = 1 / pow(10, $float_precision);

                $resolver = new \Math\Calculator\MatrixResolverQR($this->companionMatrix(), $qr_epsilon, $qr_max_iteration);
                $resolver->calculate();
                $ev = $resolver->getEigenvalues();

                $roots = array();
                $multiplicities = array();
                for ($i = 0; $i < count($ev); ++$i) {
                    if (!is_numeric($ev[$i]) && $ev[$i] instanceof Complex) {
                        $ev[$i] = "(" . $ev[$i]->toString($float_precision) . ")";
                        if (empty($roots)) {
                            $roots[] = $ev[$i];
                            $multiplicities[] = 1;
                        } else {
                            if ($roots[$i - 1] == $ev[$i]) {
                                $multiplicities[$i - 1]++;
                            } else {
                                $roots[] = $ev[$i];
                                $multiplicities[] = 1;
                            }
                        }
                    } else {
                        $ev[$i] = round($ev[$i], $float_precision);
                        if (empty($roots)) {
                            $roots[] = $ev[$i];
                            $multiplicities[] = 1;
                        } else {
                            if (abs($roots[$i - 1] - $ev[$i]) < $roots_precision) {
                                $multiplicities[$i - 1]++;
                            } else {
                                $roots[] = $ev[$i];
                                $multiplicities[] = 1;
                            }
                        }
                    }
                }
                $this->roots = $roots;
                $this->multiplicities = $multiplicities;
                $this->getScalarFactor();
                break;
            default:
                ;
        }
    }

    public function expandAndSimplify()
    {
        $scalar_factor = $this->getScalarFactor();
        if ($this->getDegree() == 1) {
            $x0 = $this->getRoot(0);
            $this->coefficients = array($scalar_factor * (-$x0), $scalar_factor * (1));
        } elseif ($this->getDegree() == 2) {
            $x0 = $this->getRoot(0);
            if ($this->getMultiplicity(0) == 2) {
                $x1 = $this->getRoot(0);
            } else {
                $x1 = $this->getRoot(1);
            }
            $this->coefficients = array($x0 * $x1, -$x0 -$x1, 1);
        } elseif ($this->getDegree() == 3) {
            $x0 = $this->getRoot(0);
            if ($this->getMultiplicity(0) >= 2) {
                $x1 = $this->getRoot(0);
                if ($this->getMultiplicity(0) == 3) {
                    $x2 = $this->getRoot(0);
                } else {
                    $x2 = $this->getRoot(1);
                }
            } else {
                $x1 = $this->getRoot(1);
                if ($this->getMultiplicity(1) == 2) {
                    $x2 = $this->getRoot(1);
                } else {
                    $x2 = $this->getRoot(2);
                }
            }
            $this->coefficients = array($scalar_factor * (-$x0 * $x1 * $x2), $scalar_factor * ($x0 * $x1 + $x1 * $x2 + $x0 * $x2), $scalar_factor * (-$x0 - $x1 - $x2), $scalar_factor * (1));
            // (-1)^3(X - x0)(X - x1)(X - x2) = (-1) * (X^3 + (-x0 - x1 - x2) * X^2 + (x0x1 + x1x2 + x0x2) * X + (-x0x1x2))
        }
    }

    /** @return \Math\Matrix **/
    public function companionMatrix()
    {
        $unit_coefficients = array();
        for ($i = $this->getDegree(); $i >= 0 ; --$i) {
            $unit_coefficients[$i] = $this->getCoefficient($i) / $this->getCoefficient($this->getDegree());
        }

        $companion_arr = array(array());
        for ($i = 0; $i < $this->getDegree(); ++$i) {
            for ($j = 0; $j < $this->getDegree(); ++$j) {
                if ($i == $j + 1) {
                    $companion_arr[$i][$j] = 1;
                } elseif ($j == $this->getDegree() - 1) {
                    $companion_arr[$i][$j] = -$unit_coefficients[$i];
                } else {
                    $companion_arr[$i][$j] = 0;
                }
            }
        }

        return new Matrix($companion_arr);
    }

    /** @return \Math\Polynomial**/
    public function operatorAdd(Polynomial $q)
    {
        return self::add($this, $q, $this->getInputType(), $this->getOutputType());
    }

    /** @return \Math\Polynomial**/
    public function operatorSubtract(Polynomial $q)
    {
        return self::subtract($this, $q, $this->getInputType(), $this->getOutputType());
    }

    /** @return \Math\Polynomial**/
    public function operatorMultiply(Polynomial $q)
    {
        return self::multiply($this, $q, $this->getInputType(), $this->getOutputType());
    }

    /** @return \Math\Polynomial**/
    public function operatorEuclideanDivide(Polynomial $q)
    {
        return self::euclideanDivide($this, $q, $this->getInputType(), $this->getOutputType());
    }

    /** @return \Math\Polynomial**/
    public function operatorScalarMultiply($k)
    {
        return self::scalarMultiply($this, $k, $this->getInputType(), $this->getOutputType());
    }

    /*-------------------------------------`
     * Private methods                     *
     `-------------------------------------*/

    private function verifyType($var, $attribute = 'output_type')
    {
        switch ($this->$attribute) {
            case 'numeric':
            case 'float':
            case 'double':
                $f = function($var) {
                    return is_numeric($var) && (is_int($var) || is_float($var) || is_double($var) || ($var == (string)(float)$var) || ($var == (string)(int)$var));
                };
                break;
            case 'integer':
                $f = function($var) {
                    return is_numeric($var) && (is_int($var) || ($var == (string)(int)$var));
                };
                break;
            default:
                $f = function($var) { return false; };
        }
        return call_user_func($f, $var);
    }
}