<?php
namespace Math;


class Complex
{
    protected $x = 0.0;
    protected $y = 0.0;
    protected $polar_form_flag = false;

    /*-------------------------------------`
     * Static methods                      *
     `-------------------------------------*/
    /*-------------------------------------`
     * Accessors / Mutators                *
     `-------------------------------------*/

    public function re()
    {
        // @TODO
        return $this->polar_form_flag ? null : $this->x;
    }

    public function im()
    {
        // @TODO
        return $this->polar_form_flag ? null : $this->y;
    }

    public function mod()
    {
        // @TODO
        return $this->polar_form_flag ? null : sqrt($this->re * $this->re + $this->im * $this->im);
    }

    public function arg()
    {
        // @TODO
        return $this->polar_form_flag ? null : atan2($this->im, $this->re);
    }

    public function isPolarForm()
    {
        return $this->polar_form_flag;
    }

    public function isCartesianForm()
    {
        return !$this->polar_form_flag;
    }

    /*-------------------------------------`
     * Constructor && Magic methods        *
     `-------------------------------------*/
    public function __construct($x, $y, $polar_form = false)
    {
        $this->x = $x;
        $this->y = $y;
        $this->polar_form_flag = $polar_form;
    }

    public function __toString()
    {
        $str = $this->re();
        $str .= $this->im() >=0 ? "+" : "-";
        $str .= abs($this->im()) . "i";
        return $str;
    }

    /*-------------------------------------`
     * Specifics methods                   *
     `-------------------------------------*/
    public function equalsTo($complex_or_array, $epsilon = 0.001)
    {
        if (is_array($complex_or_array)) {
            if (isset($complex_or_array[2]) && $complex_or_array[2]) { // polar form
                return (abs($complex_or_array[0] - $this->mod()) < $epsilon) && (abs($complex_or_array[1] - $this->arg()) < $epsilon);
            } else {
                return (abs($complex_or_array[0] - $this->re()) < $epsilon) && (abs($complex_or_array[1] - $this->im()) < $epsilon);
            }
        }
        if ($complex_or_array instanceof Complex) {
            return (abs($complex_or_array->re() - $this->re()) < $epsilon) && (abs($complex_or_array->im() - $this->im()) < $epsilon);
        }
        return false;
    }

    public function toString($float_precision = 0.001)
    {
        $str = round($this->re(), $float_precision);
        $str .= $this->im() >=0 ? "+" : "-";
        $str .= abs(round($this->im(), $float_precision)) . "i";
        return $str;
    }
}
