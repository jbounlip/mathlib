<?php
namespace Math\Calculator;

use Math\Matrix;

class MatrixDecompositionQRHouseholder implements ICalculator
{
    protected $matrix = null;
    protected $calculated_flag = false;
    protected $q = null;
    protected $r = null;

    /*-------------------------------------`
     * Accessors / Mutators                *
     `-------------------------------------*/

    public function resetMatrix(Matrix $matrix)
    {
        $this->matrix = $matrix;
        $this->calculated_flag = false;
        $this->q = null;
        $this->r = null;
    }

    public function isCalculated()
    {
        return $this->calculated_flag;
    }

    /** @return \Math\Matrix|null */
    public function getQ()
    {
        return $this->calculated_flag ? $this->q : null;
    }

    /** @return \Math\Matrix|null */
    public function getR()
    {
        return $this->calculated_flag ? $this->r : null;
    }

    /*-------------------------------------`
     * Constructor && Magic methods        *
     `-------------------------------------*/
    public function __construct(Matrix $matrix)
    {
        $this->resetMatrix($matrix);
    }

    /*-------------------------------------`
     * Specifics methods                   *
     `-------------------------------------*/
    public function calculate()
    {
        if ($this->matrix->isValid()) {
            $m = $this->matrix->getRowsCount();
            $n = $this->matrix->getColumnsCount();
            $identity = Matrix::identity($m);

            $a_arr = $this->matrix->toArray();
            $h_arr = $identity->toArray();

            for ($k = 0; $k < $m - 1; ++$k) {
                // alpha
                $alpha = 0;
                for ($i = $k; $i < $m; ++$i) {
                    $alpha += $a_arr[$i][$k] * $a_arr[$i][$k];
                }
                $alpha = ($a_arr[$k][$k] < 0) ? sqrt($alpha) : -sqrt($alpha); //ERROR wiki: \alpha should get the opposite sign as the k-th coordinate of \mathbf{x}
                $beta = $alpha * $alpha - $alpha * $a_arr[$k][$k];
                // V_k
                $v_arr = array();
                for ($i = 0; $i < $m; ++$i) {
                    $v_arr[$i] = 0;
                }
                $v_arr[$k] = $a_arr[$k][$k] - $alpha;
                for ($i = $k + 1; $i < $m; ++$i) {
                    $v_arr[$i] = $a_arr[$i][$k];
                }
                // A_k+1
                for ($j = $k; $j < $n; ++$j) {
                    $c = 0;
                    for ($i = $k; $i < $m; ++$i) {
                        $c += $v_arr[$i] * $a_arr[$i][$j];
                    }
                    $c = $c / $beta;
                    for ($i = $k; $i < $m; ++$i) {
                        $a_arr[$i][$j] = $a_arr[$i][$j] - $c * $v_arr[$i];
                    }
                }
                //H
                for ($j = 0; $j < $m; ++$j) {
                    $c = 0;
                    for ($i = $k; $i < $m; ++$i) {
                        $c += $v_arr[$i] * $h_arr[$i][$j];
                    }
                    $c = $c / $beta;
                    for ($i = $k; $i < $m; ++$i) {
                        $h_arr[$i][$j] = $h_arr[$i][$j] - $c * $v_arr[$i];
                    }
                }
            }

            // Force triangularization
            for ($i = 0; $i < $m; ++$i) {
                for ($j = 0; $j < $i; ++$j) {
                    $a_arr[$i][$j] = 0;
                }
            }

            $this->q = Matrix::transpose(new Matrix($h_arr));
            $this->r = new Matrix($a_arr);
            $this->calculated_flag = true;
        }
    }
}
