<?php
namespace Math\Calculator;

use Math\Matrix;

class MatrixResolverQR implements ICalculator
{
    protected $matrix = null;
    protected $calculated_flag = false;
    protected $has_complex_eigenvalues = false;
    protected $epsilon = 0.000001;
    protected $max_iteration = 100;
    protected $float_precision = 3;
    protected $eigenvalues = array();
    protected $eigenvectors = array();

    /*-------------------------------------`
     * Accessors / Mutators                *
     `-------------------------------------*/

    public function isCalculated()
    {
        return $this->calculated_flag;
    }

    public function getEigenvalues()
    {
        return $this->eigenvalues;
    }

    /*-------------------------------------`
     * Constructor && Magic methods        *
     `-------------------------------------*/
    public function __construct(Matrix $matrix, $epsilon = 0.000001, $max_iteration = 100)
    {
        if ($matrix->isSquare()) {
            $this->matrix = $matrix;
            $this->epsilon = $epsilon;
            $this->max_iteration = $max_iteration;
        }
    }

    /*-------------------------------------`
     * Specifics methods                   *
     `-------------------------------------*/
    public function isValid()
    {
        return !is_null($this->matrix);
    }

    public function calculate()
    {
        if ($this->isValid()) {
            $RQ = $this->matrix;
            $decomposer = new MatrixDecompositionQRHouseholder($RQ);
            for ($k = 0; $k < $this->max_iteration && $this->maxAbsTriSubElement($RQ) > $this->epsilon; ++$k) {
                $decomposer->calculate();
                if (!$decomposer->isCalculated()) {
                    return ;
                }
                $RQ = Matrix::multiply($decomposer->getR(), $decomposer->getQ());
                $decomposer->resetMatrix($RQ);
            }

            for ($i = 0; $i < $RQ->getRowsCount(); ++$i) {
                $this->eigenvalues[$i] = $RQ($i, $i);
            }
            if ($k >= $this->max_iteration) {
                $epsilon_float = 1.0 / pow(10, $this->float_precision);
                // si ne converge pas (valeurs propres complexes?)
                // @TODO: Matrix::eigenvalues a renvoyer reel ou complex/ou exclue les racines multiples
                for ($i = 1; $i < $RQ->getRowsCount(); $i += 2) {
                    if (abs($RQ($i, $i - 1)) > $this->epsilon) {
                        $local_matrix = new Matrix([
                            [$RQ($i - 1, $i - 1), $RQ($i - 1, $i)],
                            [$RQ($i, $i - 1), $RQ($i, $i)],
                        ]);
                        $local_matrix_ev = $local_matrix->eigenvalues($epsilon_float);
                        $this->eigenvalues[$i - 1] = $local_matrix_ev[0];
                        $this->eigenvalues[$i] = $local_matrix_ev[1];
                    }
                }
            }
            // @TODO: eigenvectors

            $this->calculated_flag = true;
        }
    }

    /*-------------------------------------`
     * Private methods                   *
     `-------------------------------------*/
    private function maxAbsTriSubElement(Matrix $m)
    {
        $max = abs($m(1, 0));
        for ($i = 1; $i < $m->getRowsCount(); ++$i) {
            for ($j = 0; $j < $i; ++$j) {
                if ($i != $j) {
                    if (abs($m($i, $j)) > $max) {
                        $max = abs($m($i, $j));
                    }
                }
            }
        }
        return $max;
    }
}
