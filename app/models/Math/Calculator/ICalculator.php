<?php
namespace Math\Calculator;

interface ICalculator
{
    public function calculate();
    public function isCalculated();
}
