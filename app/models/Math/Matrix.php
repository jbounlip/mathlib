<?php
//
// Matrix.php for  in /Users/jb/proj/etna/mat2/bounli_j/inc
// 
// Made by BOUNLIPHONE Jacques
// Login   <bounli_j@etna-alternance.net>
// 
// Started on  Tue Jan  7 13:46:52 2014 BOUNLIPHONE Jacques
// Last update Fri Jan 16 17:15:44 2015 BOUNLIPHONE Jacques
//
namespace Math;

/**
 * Classe Matrice
 */
class Matrix
{
    protected $rows_count = 0;
    protected $columns_count = 0;
    protected $data = array();
    protected $extra_params = array(
        'input_type' => 'float',
        'output_type' => 'float',
        'integer_roots_try_from' => -10,
        'integer_roots_try_to' => 10,
        'float_qr_epsilon' => 0.000001,
        'float_qr_max_iteration' => 100,
        'float_precision' => 3,
    );

    /*-------------------------------------`
     * Static methods                      *
     `-------------------------------------*/

    public static function identity($degree)
    {
        if ($degree > 1) {
            $elements = array(array());
            for ($i = 0; $i < $degree; ++$i) {
                for ($j = 0; $j < $degree; ++$j) {
                    $elements[$i][$j] = 0;
                }
            }
            for ($i = 0; $i < $degree; ++$i) {
                $elements[$i][$i] = 1;
            }
            return new Matrix($elements);
        }
        return null;
    }

    public static function transpose(Matrix $m)
    {
        if ($m->isValid()) {
            $matrix_array = array(array());
            for ($i = 0; $i < $m->rows_count; ++$i) {
                for ($j = 0; $j < $m->columns_count; ++$j) {
                    $matrix_array[$j][$i] = $m($i, $j);
                }
            }
            return new Matrix($matrix_array);
        }
        return null;
    }

    public static function add(Matrix $a, Matrix $b)
    {
        if ($a->canAddTo($b)) {
            $matrix_array = array(array());
            for ($i = 0; $i < $a->rows_count; ++$i) {
                for ($j = 0; $j < $a->columns_count; ++$j) {
                    $matrix_array[$i][$j] = $a($i, $j) + $b($i, $j);
                }
            }
            return new Matrix($matrix_array);
        }
        return null;
    }

    public static function scalarMultiply($k, Matrix $m)
    {
        if ($m->isValid() && is_numeric($k)) {
            $matrix_array = array(array());
            for ($i = 0; $i < $m->rows_count; ++$i) {
                for ($j = 0; $j < $m->columns_count; ++$j) {
                    $matrix_array[$i][$j] = $k * $m($i, $j);
                }
            }
            return new Matrix($matrix_array);
        }
        return null;
    }

    public static function multiply(Matrix $a, Matrix $b)
    {
        if ($a->canMultiplyTo($b)) {
            $sum_product_func = function($u, $v) {
                $result = 0;
                for ($i = 0; $i < count($u); ++$i) {
                    $result += $u[$i] * $v[$i];
                }
                return $result;
            };
            $matrix_array = array(array());
            for ($i = 0; $i < $a->rows_count; ++$i) {
                for ($j = 0; $j < $b->columns_count; ++$j) {
                    $matrix_array[$i][$j] = $sum_product_func($a->rowVectorArray($i), $b->columnVectorArray($j));
                }
            }
            return new Matrix($matrix_array);
        }
        return null;
    }

    /*-------------------------------------`
     * Accessors / Mutators                *
     `-------------------------------------*/
    public function getRowsCount()
    {
        return ($this->rows_count);
    }

    public function getColumnsCount()
    {
        return ($this->columns_count);
    }

    public function rowVectorArray($i)
    {
        return ($this->data[$i]);
    }

    public function columnVectorArray($j)
    {
        $column_vector = array();
        for ($i = 0; $i < $this->rows_count; ++$i) {
            $column_vector[] = $this->data[$i][$j];
        }
        return ($column_vector);
    }

    public function toArray()
    {
        return ($this->data);
    }

    public function getExtraParams()
    {
        return $this->extra_params;
    }

    public function setExtraParams(array $extra_params)
    {
        $this->extra_params = $extra_params;
    }

    public function getExtraParam($key)
    {
        return isset($this->extra_params[$key]) ? $this->extra_params[$key] : null;
    }

    public function setExtraParam($key, $value)
    {
        $this->extra_params[$key] = $value;
    }

    /*-------------------------------------`
     * Constructor && Magic methods        *
     `-------------------------------------*/
    public function __construct(array $matrix_array, $input_type = 'float', $output_type = 'float')
    {
        if (isset($matrix_array[0]) && is_array($matrix_array[0])) {
            $this->rows_count = count($matrix_array);
            $this->columns_count = count($matrix_array[0]);
            $this->data = $matrix_array;
        }
    }

    public function __invoke($i, $j)
    {
        return isset($this->data[$i][$j]) ? $this->data[$i][$j] : null;
    }

    public function __toString()
    {
        if (!$this->isValid()) {
            return ("Bad matrix format");
        }
        $str = "";
        for ($i = 0; $i < $this->rows_count; ++$i) {
            $str .= "[";
            for ($j = 0; $j < $this->columns_count; ++$j) {
                $str .= $this->data[$i][$j] . ", ";
            }
            $str = substr($str, 0, count($str) - 3);
            $str .= "]\n";
        }
        return ($str);
    }

    /*-------------------------------------`
     * Specifics methods                   *
     `-------------------------------------*/

    public function verifyInputType($var)
    {
        return $this->verifyType($var, 'input_type');
    }

    public function verifyOutputType($var)
    {
        return $this->verifyType($var, 'output_type');
    }

    public function isValid()
    {
        if (empty($this->data) || $this->rows_count <= 0 || $this->columns_count <= 0) {
            return false;
        }
        $expected_columns_count = $this->columns_count;
        foreach ($this->data as $row_vector_array) {
            if (count($row_vector_array) != $expected_columns_count) {
                return false;
            }
            foreach ($row_vector_array as $element) {
                if (!is_numeric($element) && !($element instanceof Complex)) {
                    return false;
                }
            }
        }
        return true;
    }

    public function isElementsInteger()
    {
        foreach ($this->data as $row_vector_array) {
            foreach ($row_vector_array as $element) {
                if (!is_integer($element)) {
                    return false;
                }
            }
        }
        return true;
    }

    public function isElementsReal()
    {
        foreach ($this->data as $row_vector_array) {
            foreach ($row_vector_array as $element) {
                if (!is_numeric($element)) {
                    return false;
                }
            }
        }
        return true;
    }

    public function isElementsComplex()
    {
        foreach ($this->data as $row_vector_array) {
            foreach ($row_vector_array as $element) {
                if (!is_numeric($element) && !($element instanceof Complex)) {
                    return false;
                }
            }
        }
        return true;
    }

    public function hasElementInteger()
    {
        foreach ($this->data as $row_vector_array) {
            foreach ($row_vector_array as $element) {
                if (is_integer($element)) {
                    return true;
                }
            }
        }
        return false;
    }

    public function hasElementReal()
    {
        foreach ($this->data as $row_vector_array) {
            foreach ($row_vector_array as $element) {
                if (is_float($element) || is_double($element)) {
                    return true;
                }
            }
        }
        return false;
    }

    public function hasElementComplex()
    {
        foreach ($this->data as $row_vector_array) {
            foreach ($row_vector_array as $element) {
                if ($element instanceof Complex) {
                    return true;
                }
            }
        }
        return false;
    }

    public function canAddTo(Matrix $m) {
        if ($this->isValid() && $m->isValid()) {
            if ($this->rows_count === $m->rows_count && $this->columns_count === $m->columns_count) {
                return true;
            }
        }
        return false;
    }

    public function canMultiplyTo(Matrix $m) {
        if ($this->isValid() && $m->isValid()) {
            if ($this->columns_count === $m->rows_count) {
                return true;
            }
        }
        return false;
    }

    public function equalsTo($matrix_or_array, $epsilon = 0.001)
    {
        if (is_array($matrix_or_array)) {
            $access_element_func = function(&$matrix_or_array, $i, $j) {
                return isset($matrix_or_array[$i][$j]) ? $matrix_or_array[$i][$j] : null;
            };
        } else if ($matrix_or_array instanceof Matrix) {
            $access_element_func = function(&$matrix_or_array, $i, $j) {
                return $matrix_or_array($i, $j);
            };
        } else {
            return false;
        }
        for ($i = 0; $i < $this->rows_count; ++$i) {
            for ($j = 0; $j < $this->columns_count; ++$j) {
                if (is_null($access_element_func($matrix_or_array, $i, $j))) {
                    return false;
                } elseif (abs($this->data[$i][$j] - $access_element_func($matrix_or_array, $i, $j)) > $epsilon) {
                    return false;
                }
            }
        }
        return true;
    }

    public function isSquare()
    {
        return ($this->rows_count == $this->columns_count);
    }

    public function isInvertible()
    {
        //@TODO
        return ($this->isValid() && $this->isSquare() && true);
    }

    public function trace()
    {
        if ($this->isSquare()) {
            $trace = 0;
            for ($i = 0; $i < $this->rows_count; ++ $i) {
                $trace += $this->data[$i][$i];
            }
            return $trace;
        }
        return null;
    }

    public function swapRows($r1, $r2)
    {
        for ($i = 0; $i < $this->columns_count; ++$i) {
            $tmp = $this->data[$r1][$i];
            $this->data[$r1][$i] = $this->data[$r2][$i];
            $this->data[$r2][$i] = $tmp;
        }
    }

    /** Faddeev-Leverrier Algorithm **/
    public function characteristicPolynomial()
    {
        if ($this->isValid() && $this->isSquare()) {
            if ($this->rows_count == 3 && $this->getExtraParam('output_type') == 'integer') { // matrice carrée d'ordre 3 et valeurs propres entières
                $identity_matrix = Matrix::identity(3);
                $factor_matrix = array();
                $factor_matrix[0] = $this;
                $factor_matrix[1] = Matrix::multiply(
                    $factor_matrix[0],
                    Matrix::add(
                        $factor_matrix[0],
                        Matrix::scalarMultiply(
                            -1 * $factor_matrix[0]->trace(),
                            $identity_matrix
                        )
                    )
                );
                $factor_matrix[2] = Matrix::multiply(
                    $factor_matrix[1],
                    Matrix::add(
                        $factor_matrix[0],
                        Matrix::scalarMultiply(
                            -0.5 * $factor_matrix[0]->trace(),
                            $identity_matrix
                        )
                    )
                );
                return new Polynomial(array(
                    $factor_matrix[2]->trace() / 3,
                    $factor_matrix[1]->trace() / 2,
                    $factor_matrix[0]->trace(),
                    -1,
                ), false, $this->getExtraParam('input_type'), $this->getExtraParam('output_type'));
            } else {
                $sign = $this->rows_count % 2 == 0 ? 1 : -1;
                $coefficients_arr = array();
                $identity_matrix = Matrix::identity($this->rows_count);
                $factor_matrix = $this;

                for ($k = 1.0; $k <= $factor_matrix->rows_count; ++$k) {
                    array_unshift($coefficients_arr, -$sign * $factor_matrix->trace() / $k);
                    $factor_matrix = Matrix::multiply(
                        $this,
                        Matrix::add(
                            $factor_matrix,
                            Matrix::scalarMultiply(
                                (-1.0 / $k) * $factor_matrix->trace(),
                                $identity_matrix
                            )
                        )
                    );
                }
                array_push($coefficients_arr, $sign);
                return new Polynomial($coefficients_arr, false, $this->getExtraParam('input_type'), $this->getExtraParam('output_type'));
            }
        }
        return null;
    }

    public function eigenvalues($epsilon = null)
    {
        if (is_null($epsilon)) {
            $epsilon = 1 / pow(10, $this->getExtraParam('float_precision'));
        }
        // @TODO: change return type (Real or Complex)
        if ($this->isValid() && $this->isSquare()) {
            if ($this->rows_count == 2) {
                $a = $this->data[0][0];
                $b = $this->data[0][1];
                $c = $this->data[1][0];
                $d = $this->data[1][1];
                $x = ($a + $d) / 2;
                $delta = ($a - $d) * ($a - $d) + 4 * $b * $c;
                if ($delta + $epsilon < 0) {
                    return array(
                        new Complex($x, sqrt(abs($delta)) / 2),
                        new Complex($x, -sqrt(abs($delta)) / 2),
                    );
                } else {
                    return array(
                        $x + sqrt(abs($delta)) / 2,
                        $x - sqrt(abs($delta)) / 2,
                    );
                }
            } else { // QR algorithm
                $resolver = new \Math\Calculator\MatrixResolverQR($this);
                $resolver->calculate();
                return $resolver->getEigenvalues();
            }
        }
        return null;
    }

    /*-------------------------------------`
     * Private methods                     *
     `-------------------------------------*/

    private function verifyType($var, $extra_param_key = 'output_type')
    {
        switch ($this->getExtraParam($extra_param_key)) {
            case 'numeric':
            case 'float':
                $f = function($var) {
                    return is_numeric($var) && (($var == (string)(float)$var) || ($var == (string)(int)$var));
                };
                break;
            case 'integer':
                $f = function($var) {
                    return is_numeric($var) && ($var == (string)(int)$var);
                };
                break;
            default:
                $f = function($var) { return false; };
        }
        return call_user_func($f, $var);
    }

}
