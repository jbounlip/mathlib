<?php

namespace Math;

class Monomial extends Polynomial
{
    public static function divide(Monomial $p, Monomial $q, $input_type = 'float', $output_type = 'float')
    {
        if ($p->getDegree() < $q->getDegree()) {
            return null;
        }
        $degree = $p->getDegree() - $q->getDegree();
        $lead_coefficient = $p->leadCoefficient() / $q->leadCoefficient();
        return new Monomial($lead_coefficient, $degree, $input_type, $output_type);
    }

    public static function polynomialMultiply(Monomial $mono, Polynomial $q, $input_type = 'float', $output_type = 'float')
    {
        $res = array();
        for ($i = 0; $i < $mono->getDegree(); ++$i) {
            $res[] = 0;
        }
        for ($i = 0; $i <= $q->getDegree(); ++$i) {
            $res[] = $mono->leadCoefficient() * $q->getCoefficient($i);
        }
        return new Polynomial($res);
    }

    public function __construct($lead_coefficient, $degree, $input_type = 'float', $output_type = 'float')
    {
        if (is_numeric($degree)) { //@TODO
            $data = array();
            for ($i = 0; $i < $degree; ++$i) {
                $data[$i] = 0;
            }
            $data[$i] = $lead_coefficient;
            parent::__construct($data, false, $input_type, $output_type);
        }
    }

    public function operatorDivide(Monomial $q)
    {
        return self::divide($this, $q, $this->getInputType(), $this->getOutputType());
    }

    public function operatorPolynomialMultiply(Polynomial $q)
    {
        return self::polynomialMultiply($this, $q, $this->getInputType(), $this->getOutputType());
    }
} 