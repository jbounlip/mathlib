<?php
//
// Vector.php for  in /Users/jb/proj/etna/mat2/bounli_j/inc
// 
// Made by BOUNLIPHONE Jacques
// Login   <bounli_j@etna-alternance.net>
// 
// Started on  Thu Jan  9 11:46:27 2014 BOUNLIPHONE Jacques
// Last update Fri Jan 16 18:15:44 2015 BOUNLIPHONE Jacques
//
namespace Math;

/**
 * Classe Vecteur colonne/ligne
 */
class Vector extends Matrix
{
    const COLUMN_VECTOR = 0;
    const ROW_VECTOR = 1;

    private $column_vector_flag = true;

    /*-------------------------------------`
     * Static methods                      *
     `-------------------------------------*/

    public static function e($i, $degree, $column_vector_flag = true)
    {
        $arr = array();
        for ($k = 0; $k < $degree; ++$k) {
            $arr[$k] = 0;
        }
        $arr[$i] = 1;
        return new Vector($arr, $column_vector_flag);
    }

    public static function add(Matrix $u, Matrix $v)
    {
        if ($u instanceof Vector && $v instanceof Vector) {
            if ($u->isColumnVector()) {
                return new Vector(parent::add($u, $v)->columnVectorArray(0), true);
            } else {
                return new Vector(parent::add($u, $v)->rowVectorArray(0), false);
            }
        }
        return null;
    }

    public static function scalarMultiply($k, Matrix $v)
    {
        if ($v instanceof Vector) {
            if ($v->isValid() && is_numeric($k)) {
                $vector_array = array();
                for ($i = 0; !is_null($v($i)); ++$i) {
                    $vector_array[$i] = $k * $v($i);
                }
                return new Vector($vector_array, $v->isColumnVector());
            }
        }
        return null;
    }

    /*-------------------------------------`
     * Accessors / Mutators                *
     `-------------------------------------*/
    public function isColumnVector()
    {
        return $this->column_vector_flag;
    }

    public function isRowVector()
    {
        return !$this->column_vector_flag;
    }

    /*-------------------------------------`
     * Constructor && Magic methods        *
     `-------------------------------------*/
    public function __construct(array $vector_array, $column_vector_flag = true)
    {
        $this->column_vector_flag = $column_vector_flag;
        if ($column_vector_flag) {
            $this->rows_count = count($vector_array);
            $this->columns_count = 1;
            $this->data = array();
            for ($i = 0; $i < $this->rows_count; ++$i) {
                $this->data[] = array($vector_array[$i]);
            }
        } else {
            parent::__construct(array($vector_array));
        }
    }

    public function __invoke($i, $j = null)
    {
        if (is_null($j)) {
            if ($this->column_vector_flag) {
                return isset($this->data[$i][0]) ? $this->data[$i][0] : null;
            }
            return isset($this->data[0][$i]) ? $this->data[0][$i] : null;
        }
        return parent::__invoke($i, $j);
    }

    /*-------------------------------------`
     * Specifics methods                   *
     `-------------------------------------*/
    public function euclideanNorm()
    {
        $sum = 0;
        for ($i = 0; !is_null($this($i)); ++$i) {
            $sum += $this($i) * $this($i);
        }
        return sqrt($sum);
    }
}
