<?php

namespace Controllers;


abstract class BaseController
{
    protected $registry = array();
    protected $header_flag = false;
    protected $header_content_array = array();

    /*-------------------------------------`
     * Static methods                      *
     `-------------------------------------*/
    public static function getInstance()
    {
        static $instance;
        if (!isset($instance)) {
            $instance = new static();
        }
        return $instance;
    }

    /*-------------------------------------`
     * Constructor && Magic methods        *
     `-------------------------------------*/
    public function __construct()
    {
    }

    /*-------------------------------------`
     * Specifics methods                   *
     `-------------------------------------*/
    public function call($method_alias, $request_method, $request_args = array())
    {
        $method_name = strtolower($request_method) . ucfirst($method_alias);
        if (is_callable(array($this, $method_name))) {
            $content = call_user_func(array($this, $method_name), $request_args);
            while (!is_null($header_content = $this->getHeader())) {
                header($header_content);
            }
            echo $content;
            $this->reset();
            return true;
        }
        $this->noExistingMethod($request_args, $method_name);
        return false;
    }

    protected function assign($variable_name, $value)
    {
        $this->registry[$variable_name] = $value;
    }

    protected function assignTemplate($variable_name, $view_alias, $template_extension = 'html')
    {
        foreach ($this->registry as $key => $value) {
            $$key = $value;
        }
        ob_start();
        include PROJECT_BASE_DIR . '/app/views/' . $view_alias . '.' . $template_extension . '.php';
        $this->registry[$variable_name] = ob_get_clean();
    }

    protected function output($layout_alias, $template_extension = 'html')
    {
        $this->assignTemplate('__layout__', $layout_alias, $template_extension);
        return $this->registry['__layout__'];
    }

    protected function withHeader($header_content)
    {
        array_push($this->header_content_array, $header_content);
        $this->header_flag = true;
        return $this;
    }

    /*-------------------------------------`
     * Private methods                     *
     `-------------------------------------*/
    private function reset()
    {
        $this->registry = array();
        $this->header_flag = false;
        $this->header_content_array = array();
    }

    private function noExistingMethod($request_args, $method_name = null)
    {
        header("HTTP/1.0 404 Not Found");
        if (DEBUG_FLAG) {
            $error_msg = "erreur 404 (route " . $_SERVER['REQUEST_URI'] . " does not exist). ";
            $error_msg .= "Tried to call " . $method_name . " from " . get_class($this);
            throw new \Exception($error_msg);
        }
        return false;
    }

    private function getHeader()
    {
        return ($this->header_flag) ? array_pop($this->header_content_array) : null;
    }

}
