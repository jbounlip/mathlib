<?php
namespace Controllers;

use Filters\RequestFilter;
use Math\Matrix;
use Math\Polynomial;

class MathController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Example route: /Math/PolynomialRoots?json={"polynomial":[6,-11,6,-1],"input_type":"integer","output_type":"integer","try_from":-100,"try_to":100}
     */
    public function getPolynomialRoots($request_args)
    {
        if ($json = RequestFilter::validateJsonFormat($request_args['json'], '/{"polynomial":\[.*\],"input_type":".+","output_type":".+"(?:,"try_from":"\d+","try_to":"\d+")?}/i')) {
            $p = new \Math\Polynomial($json->polynomial, false, $json->input_type, $json->output_type);
            if (isset($json->try_from) && isset($json->try_to)) {
                $p->setExtraParam('integer_roots_try_from', $json->try_from);
                $p->setExtraParam('integer_roots_try_to', $json->try_to);
            }
            if (isset($json->precision) && isset($json->max_iteration)) {
                $p->setExtraParam('float_qr_epsilon', $json->precision);
                $p->setExtraParam('float_qr_max_iteration', $json->max_iteration);
            }
            if (isset($json->float_precision)) {
                $p->setExtraParam('float_precision', $json->float_precision);
            }
            if ($p->isValid()) {
                $this->assign('json_array', array(
                    "polynomial" => [
                        "type" => "partial",
                        "data" => [
                            "input_type" => $p->getInputType(),
                            "output_type" => $p->getOutputType(),
                            "roots" => $p->getRoots(),
                        ],
                    ],
                ));
                return $this->withHeader('Content-Type: application/json')->output('layouts/from_array', 'json');
            }
        }
        $this->assign('json_array', array("error" => "bad syntax"));
        return $this->withHeader('Content-Type: application/json')->withHeader('HTTP/1.0 400 Bad Request')->output('layouts/from_array', 'json');
    }

    /**
     * Example route: /Math/PolynomialFactorization?json={"polynomial":[-16,12,0,-1],"input_type":"integer","output_type":"integer"}
     */
    public function getPolynomialFactorization($request_args)
    {
        if ($json = RequestFilter::validateJsonFormat($request_args['json'], '/{"polynomial":\[.*\],"input_type":".*","output_type":".*"(?:,"try_from":"\d+","try_to":"\d+")?}/i')) {
            $p = new \Math\Polynomial($json->polynomial, false, $json->input_type, $json->output_type);
            if (isset($json->try_from) && isset($json->try_to)) {
                $p->setExtraParam('integer_roots_try_from', $json->try_from);
                $p->setExtraParam('integer_roots_try_to', $json->try_to);
            }
            if (isset($json->precision) && isset($json->max_iteration)) {
                $p->setExtraParam('float_qr_epsilon', $json->precision);
                $p->setExtraParam('float_qr_max_iteration', $json->max_iteration);
            }
            if (isset($json->float_precision)) {
                $p->setExtraParam('float_precision', $json->float_precision);
            }
            if ($p->isValid()) {
                $this->assign('json_array', array(
                    "polynomial" => [
                        "type" => "factorized",
                        "data" => array_merge([
                            "input_type" => $p->getInputType(),
                            "output_type" => $p->getOutputType()],
                            $p->toArray(true)
                        ),
                    ],
                ));
                return $this->withHeader('Content-Type: application/json')->output('layouts/from_array', 'json');
            }
        }
        $this->assign('json_array', array("error" => "bad syntax"));
        return $this->withHeader('Content-Type: application/json')->withHeader('HTTP/1.0 400 Bad Request')->output('layouts/from_array', 'json');
    }

    /**
     * Example route: /Math/MatrixCharacteristicPolynomial?json={"matrix":[[-1, 2, 0],[2, 2, -3],[-2, 2, 1]],"input_type":"integer","output_type":"integer"}
     */
    public function getMatrixCharacteristicPolynomial($request_args)
    {
        if ($json = RequestFilter::validateJsonFormat($request_args['json'], '/{"matrix":\[\[.*\]\],"input_type":".*","output_type":".*"(?:,"try_from":"\d+","try_to":"\d+")?}/i')) {
            $m = new Matrix($json->matrix);
            $m->setExtraParam('input_type', $json->input_type);
            $m->setExtraParam('output_type', $json->output_type);
            if (isset($json->try_from) && isset($json->try_to)) {
                $m->setExtraParam('integer_roots_try_from', $json->try_from);
                $m->setExtraParam('integer_roots_try_to', $json->try_to);
            }
            if (isset($json->precision) && isset($json->max_iteration)) {
                $m->setExtraParam('float_qr_epsilon', $json->precision);
                $m->setExtraParam('float_qr_max_iteration', $json->max_iteration);
            }
            if (isset($json->float_precision)) {
                $m->setExtraParam('float_precision', $json->float_precision);
            }

            if ($m->isValid()) {
                $characteristic_polynomial = $m->characteristicPolynomial();
                $this->assign('json_array', array(
                    "matrix" => [
                        "characteristic_polynomial" => [
                            "type" => "full",
                            "data" => array_merge([
                                "input_type" => $characteristic_polynomial->getInputType(),
                                "output_type" => $characteristic_polynomial->getOutputType()],
                                array_merge(
                                    ["coefficients" => $characteristic_polynomial->toArray(false)],
                                    $characteristic_polynomial->toArray(true)
                                )
                            ),
                        ],
                    ],
                ));
                //return $this->output('layouts/from_array', 'json');
                return $this->withHeader('Content-Type: application/json')->output('layouts/from_array', 'json');
            }
        }
        $this->assign('json_array', array("error" => "bad syntax"));
        return $this->withHeader('Content-Type: application/json')->withHeader('HTTP/1.0 400 Bad Request')->output('layouts/from_array', 'json');
    }

}