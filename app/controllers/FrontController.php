<?php
namespace Controllers;

class FrontController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    // accessible via
    // http://localhost:24242
    // http://localhost:24242/
    // http://localhost:24242/front
    public function getIndex()
    {
        //$this->assign('toto', 454354534435);
        $this->assignTemplate('content', 'front/main');
        return $this->output('layouts/master');
    }

    // accessible via
    // http://localhost:24242/front/test
    public function getTest()
    {
        return $this->output('layouts/test');
    }

    // accessible via
    // http://localhost:24242/front/jsontest
    public function getJsonTest()
    {
        $this->assign('json_array', array(
            1, 2, [ 'yo' => 42, 'nose' => -32 * 443], 'FUUU'
        ));
        return $this->withHeader('Content-Type: application/json')->output('layouts/from_array', 'json');
    }

    public function toto()
    {
        $this->assign('toto', 45646);
        $this->assign('ttttto', 4560);
    }
}
