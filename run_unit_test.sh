#!/bin/bash

## Install dev dependencies like phpunit
#if ! [ -d test/vendor -a -f test/vendor/bin/phpunit ]
#then
    cd test && composer install --dev && cd -
#fi

## run phpunit
cd test && vendor/bin/phpunit --verbose --debug
