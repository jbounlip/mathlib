var unknown_var_letter = "X";

function displayMath(input, outnode) {
    var n = outnode.childNodes.length;
    for (var i = 0; i < n; ++i) {
        outnode.removeChild(outnode.firstChild);
    }
    outnode.appendChild(document.createTextNode("amath " + input));
    AMprocessNode(outnode);
}

function factorizedPolynomialToString(factorized_polynomial, unknown_var) {
    var str = "";

    str += (factorized_polynomial.scalar_factor == 1) ? "" : ((factorized_polynomial.scalar_factor == -1) ? "-" : factorized_polynomial.scalar_factor);

    for (var i = 0; i < factorized_polynomial.roots.length; ++i) {
        if (factorized_polynomial.roots[i] == 0) {
            str += unknown_var;
        } else {
            str += "(" + unknown_var;
            str += (factorized_polynomial.roots[i] < 0) ? "+" + -factorized_polynomial.roots[i] : "-" + factorized_polynomial.roots[i];
            str += ")";
        }
        if (factorized_polynomial.multiplicities[i] != 1) {
            str += "^" + factorized_polynomial.multiplicities[i];
        }
    }
    return (str);
}

function canonicalPolynomialToString(canonical_polynomial, unknown_var) {
    var str = "", i;

    if (Array.isArray(canonical_polynomial)) {
        if (canonical_polynomial.length > 0) {
            if (canonical_polynomial.length > 1) {
                // First displayable coefficient
                i = canonical_polynomial.length - 1;
                str += (canonical_polynomial[i] == 1) ? "" : ((canonical_polynomial[i] == -1) ? "-" : canonical_polynomial[i]);
                str += unknown_var + "^" + i;

                for (--i ; i > 1; --i) {
                    if (canonical_polynomial[i] != 0) {
                        str += (canonical_polynomial[i] < 0) ? "" : "+";
                        str += (canonical_polynomial[i] == 1) ? "" : ((canonical_polynomial[i] == -1) ? "-" : canonical_polynomial[i]);
                        str += unknown_var + "^" + i;
                    }
                }
                // Second to last displayable coefficient
                str += (canonical_polynomial[1] < 0) ? "" : "+";
                str += (canonical_polynomial[1] == 1) ? "" : ((canonical_polynomial[1] == -1) ? "-" : canonical_polynomial[1]);
                str += unknown_var;
            }
            // Last displayable coefficient
            if (canonical_polynomial[0] != 0) {
                str += (canonical_polynomial[0] < 0) ? "" : "+";
                str += canonical_polynomial[0];
            }
        }
    }

    return (str);
}

function changeInputType(domain) {
    if (domain == "integer") {
        $("#div-poly input").attr('pattern', '^-?\\d+$');
        $("#div-matrix input").attr('pattern', '^-?\\d+$');
    } else if (domain == "float") {
        $("#div-poly input").attr('pattern', '^-?\\d+(?:\\.\\d+)?$');
        $("#div-matrix input").attr('pattern', '^-?\\d+(?:\\.\\d+)?$');
    }
}

function changeOutputType(domain) {
    if (domain === "integer") {
        document.getElementById("only-output-integer").hidden = false;
    } else {
        document.getElementById("only-output-integer").hidden = true;
    }
}

function generateMatrixInputs(n) {
    var step = parseInt($("#input-step").val());
    //Generates the table
    var tbdy = document.createElement('tbody');
    for(var h = 0; h < n; h++) {
        var tr=document.createElement('tr');
        for(var w = 0; w < n; w++){
            var td = document.createElement('td');
            var input = document.createElement('input');
            input.name = "mat-coeff-" + h + "-" + w;
            input.id = "input-mat-coeff-" + h + "-" + w;
            input.type = "text";
            input.pattern = '^-?\\d+$';
            input.required = 'required';
            if (step != 3) {
                input.disabled = 'disabled';
            }
            td.appendChild(input);
            tr.appendChild(td);
        }
        tbdy.appendChild(tr);
    }
    document.getElementById("main-matrix").innerHTML = '';
    document.getElementById("main-matrix").appendChild(tbdy);

    var domain = $("select-input-type").val();
    changeInputType(domain);
}

function generatePolyInputs(n) {
    var step = parseInt($("input-step").val());
    document.getElementById("main-poly").innerHTML = '';
    for(var i = n; i >= 0; i--) {
        var label = document.createElement('label');
        label.for = "input-poly-coeff-" + i;
        if (i != n) {
            label.innerHTML = "+";
        } else {
            label.innerHTML = "P(X) = ";
        }

        var div = document.createElement('div');
        div.className = "input-group";

        var input = document.createElement('input');
        input.name = "poly-coeff-" + i;
        input.id = "input-poly-coeff-" + i;
        input.type = "text";
        input.className = "form-control";
        input.pattern = '^-?\\d+$';
        input.required = 'required';
        if (step == 3) {
            input.disabled = 'disabled';
        }
        div.appendChild(input);

        if (i != 0) {
            var span = document.createElement('span');
            span.className = "input-group-addon";
            if (i != 1) {
                span.innerHTML = "X<sup>" + i + "</sup>";
            } else {
                span.innerHTML = "X";
            }
            div.appendChild(span);
        }

        document.getElementById("main-poly").appendChild(label);
        document.getElementById("main-poly").appendChild(div);

        var domain = $("select-input-type").val();
        changeInputType(domain);
    }
}

function setAvailableOutputType(n) {
    if (n == 3) {
        var new_options = [['integer', 'N'], ['float', 'R'], ['complex', 'C']];
        changeOutputType('integer');
    } else {
        var new_options = [['float', 'R'], ['complex', 'C']];
        changeOutputType('float');
    }
    $('#select-output-type').empty();

    $('#select-output-type').append($('<option>', {
        value: new_options[0][0],
        text: new_options[0][1],
        selected: 'selected'
    }));
    for (i = 1; i < new_options.length; i++) {
        $('#select-output-type').append($('<option>', {
            value: new_options[i][0],
            text: new_options[i][1]
        }));
    }
}

function setDefaultInputValuesAndPlaceholder(n) {
    var field;
    var default_placeholder;
    var default_value;
    var default_poly_placeholder;
    var default_poly_value;
    n = parseInt(n);
    switch (n) {
        case 3:
            default_placeholder = [
                [3, 4, -1],
                [-1, 1, 1],
                [0, 3, 2]
            ];
            default_value = [
                [3, 4, -1],
                [-1, 1, 1],
                ['p', 3, 2]
            ];
            default_poly_placeholder = [-16, 12, 0, -1];
            default_poly_value = ['', '', '', ''];
            break;
        case 4:
            default_placeholder = [
                [1, 2, 3, 4],
                [4, -5, 0, 4],
                [0, 1, -1, 10],
                [-1, -8, 1, 0]
            ];
            default_value = [
                [1, 2, 3, 4],
                [4, -5, 0, 4],
                [0, 1, -1, 10],
                ['', -8, 1, 0]
            ];
            default_poly_placeholder = ['', '', '', '', ''];
            default_poly_value = ['', '', '', '', ''];
            break;
        default:
            return;

    }
    for (var i = 0; i < n; ++i) {
        for (var j = 0; j < n; ++j) {
            field = $("#input-mat-coeff-" + i + "-" + j);
            field.attr("placeholder", default_placeholder[i][j]);
            field.attr("value", default_value[i][j]);
        }
    }
    for (var i = 0; i < default_poly_placeholder.length; ++i) {
        field = $("#input-poly-coeff-" + i);
        field.attr("placeholder", default_poly_placeholder[i]);
        field.attr("value", default_poly_value[i]);
    }
}

function setRandomInputValuesAndPlaceholder() {
    var field;
    var n = parseInt($("#select-degree").val());
    var domain = $("#select-input-type").val();
    var default_value = randomBiDimensionalArray(n, -15, 15, domain);

    for (var i = 0; i < n; ++i) {
        for (var j = 0; j < n; ++j) {
            field = $("#input-mat-coeff-" + i + "-" + j);
            field.attr("value", default_value[i][j]);
        }
    }
    for (var i = 0; i < n; ++i) {
        field = $("#input-poly-coeff-" + i);
        field.attr("value", default_value[0][i]);
    }
    field = $("#input-poly-coeff-" + n);
    field.attr("value", Math.floor(Math.random() * 30 - 15));
}

function emptyInputValuesAndPlaceholder() {
    var field;
    var n = parseInt($("#select-degree").val());
    var domain = $("#select-input-type").val();

    for (var i = 0; i < n; ++i) {
        for (var j = 0; j < n; ++j) {
            field = $("#input-mat-coeff-" + i + "-" + j);
            field.attr("value", "");
        }
    }
    for (var i = 0; i <= n; ++i) {
        field = $("#input-poly-coeff-" + i);
        field.attr("value", "");
    }
}

function changeDegree(n) {
    var step = parseInt($("#input-step").val());
    if (step == 1) {
        $("#div-poly h4").text("racines entières d'un polyôme de degrée " + n);
    } else {
        $("#div-poly h4").text("factorisation d'un polyôme de degrée " + n);
    }
    $("#div-matrix h4").text("polynôme caractéristique et valeurs propres d'une matrice carrée d'ordre " + n);

    setAvailableOutputType(n);

    generateMatrixInputs(n);
    generatePolyInputs(n);

    setDefaultInputValuesAndPlaceholder(n);
}

function randomBiDimensionalArray(n, min, max, domain) {
    var arr = [[]];
    var tmp_nbr;
    for (var i = 0; i < n; ++i) {
        arr.push([]);
        for (var j = 0; j < n; ++j) {
            tmp_nbr = (Math.random() * (max - min) + min);
            if (domain == 'integer') {
                arr[i].push(Math.floor(tmp_nbr));
            } else {
                arr[i].push(tmp_nbr.toFixed(2));
            }
        }
    }
    return arr;
}


function changePhase(elmt, nb) {
    var degree = parseInt($("#select-degree").val());
    switch (nb) {
        case 1:
            $("#div-poly h3").text("Calcul polynômial");
            $("#div-poly h4").text("racines entières d'un polyôme de degré " + degree);
            break;
        case 2:
            $("#div-poly h3").text("Calcul polynômial");
            $("#div-poly h4").text("factorisation d'un polyôme de degré " + degree);
            break;
        case 3:
            $("#div-matrix h3").text("Calcul matriciel");
            $("#div-matrix h4").text("polynôme caractéristique et valeurs propres d'une matrice carrée d'ordre " + degree);
            break;
    }
    if (nb === 3) {
        $("#div-poly").addClass("hidden");
        $("#div-poly input").attr('disabled','disabled');
        $("#div-matrix").removeClass("hidden");
        $("#div-matrix input").removeAttr('disabled');
    } else {
        $("#div-poly").removeClass("hidden");
        $("#div-poly input").removeAttr('disabled');
        $("#div-matrix").addClass("hidden");
        $("#div-matrix input").attr('disabled','disabled');
    }
    $(elmt).parent().children().removeClass("active");
    $(elmt).addClass("active");

    // Change input form "step"
    $("#input-step").val(nb);
    document.getElementById("main-matrix")// Hide div "result"
    $("#div-result").addClass("hidden");
}

$("#form-math").on("submit", function(e) {
    e.preventDefault();

    var $this = $(this);
    var $step = $("#input-step").val();

    var polynomial_degree = parseInt($("#select-degree").val());
    var polynomial_coefficients = [];
    for (var i = 0; i <= polynomial_degree; ++i) {
        var tmp_input = $("#input-poly-coeff-" + i);
        if (tmp_input.val() != '') {
            // @TODO: validate integer values
            polynomial_coefficients.push(tmp_input.val());
        }
    }

    var input_type = $("#select-input-type").val();
    var output_type = $("#select-output-type").val();

    if ($("#input-integer-try-from").is(':hidden')) {
        var integer_try_from =  -10;
        var integer_try_to = 10;
    } else {
        var integer_try_from =  $("#input-integer-try-from").val();
        var integer_try_to =  $("#input-integer-try-to").val();
    }

    var square_matrix_size = parseInt($("#select-degree").val());
    var square_matrix_coefficients = [];
    for (var i = 0; i < square_matrix_size; ++i) {
        square_matrix_coefficients.push(new Array());
        for (var j = 0; j < square_matrix_size; ++j) {
            var tmp_input = $("#input-mat-coeff-" + i + "-" + j);
            if (tmp_input.val() != '') {
                // @TODO: validate integer values
                square_matrix_coefficients[i].push(tmp_input.val());
            }
        }
    }

    if ($step != 3 && polynomial_coefficients.length != polynomial_degree + 1) {
        alert('Les champs doivent être remplis');
    } else if ($step == 3 && !(
        square_matrix_coefficients[0].length === square_matrix_size
        && square_matrix_coefficients[1].length === square_matrix_size //@TODO: la validation devrait se faire tout tout n
        /*&& square_matrix_coefficients[2].length === square_matrix_size*/)) {
        alert('Les champs doivent être remplis');
    } else {
        switch ($step) {
            case '1':
                var params = JSON.stringify({
                    "polynomial": polynomial_coefficients,
                    "input_type": input_type,
                    "output_type": output_type,
                    "try_from": integer_try_from,
                    "try_to": integer_try_to,
                    "precision":"0.000001",
                    "max_iteration":"100",
                    "float_precision": "3"
                });
                var ajax_url = "/Math/PolynomialRoots?json=" + params;
                break;
            case '2':
                var params = JSON.stringify({
                    "polynomial": polynomial_coefficients,
                    "input_type": input_type,
                    "output_type": output_type,
                    "try_from": integer_try_from,
                    "try_to": integer_try_to,
                    "precision":"0.000001",
                    "max_iteration":"100",
                    "float_precision": "3"
                });
                var ajax_url = "/Math/PolynomialFactorization?json=" + params;
                break;
            case '3':
                var params = JSON.stringify({
                    "matrix": square_matrix_coefficients,
                    "input_type": input_type,
                    "output_type": output_type,
                    "try_from": integer_try_from,
                    "try_to": integer_try_to,
                    "precision":"0.000001",
                    "max_iteration":"100",
                    "float_precision": "3"
                });;
                var ajax_url = "/Math/MatrixCharacteristicPolynomial?json=" + params;
                break;
            default:
                alert('It should never happen');
        }
        $.ajax({
            url: ajax_url,
            type: 'GET',
            data: null /*$this.serialize()*/,
            success: function(response) {
                $("#div-result").removeClass("hidden");
                $p_result = $("#p-result");
                $p_result.text("");
                $p_result2 = $("#p-result2");
                $p_result2.text("");

                var p_result_text = "";
                if ($step == 1) {
                    var polynomial = response.polynomial;
                    var polynomial_roots = polynomial.data.roots;

                    switch (output_type) {
                        case "integer":
                            var ouput_type_text = "entière";
                            break;
                        case "float":
                            var ouput_type_text = "réelle";
                            break;
                        case "complex":
                            var ouput_type_text = "complexe";
                            break;
                    }
                    if (polynomial_roots.length == 0) {
                        p_result_text = "Ce polynôme ne possède pas de racine " + ouput_type_text;
                        if (output_type == "integer") {
                            p_result_text += " compris entre " + integer_try_from + " et " + integer_try_to;
                        }
                        p_result_text += ".";
                    } else if (polynomial_roots.length == 1) {
                        p_result_text = "La racine " + ouput_type_text + " de ce polynôme est " + polynomial_roots[0] + ".";
                    } else if (polynomial_roots.length == 2) {
                        p_result_text = "Les deux racines " + ouput_type_text + "s de ce polynôme sont " + polynomial_roots[0] + " et " + polynomial_roots[1] + ".";
                    } else {
                        p_result_text = "Les racines " + ouput_type_text + "s de ce polynôme sont " + polynomial_roots[0];
                        for (var i = 1; i < polynomial_roots.length; ++i) {
                            p_result_text += ", " + polynomial_roots[i];
                        }
                        p_result_text += ".";
                    }
                    $p_result.text(p_result_text);
                    $p_result2.text("");
                } else if ($step == 2) {
                    var polynomial = response.polynomial;
                    var polynomial_data = polynomial.data;
                    var polynomial_data = polynomial_data;

                    var tmp_text_original = canonicalPolynomialToString(polynomial_coefficients, unknown_var_letter);
                    var tmp_text_factorized = factorizedPolynomialToString(polynomial_data, unknown_var_letter);
                    if (polynomial_data.roots.length == 0) {
                        p_result_text = "P_(A)(" + unknown_var_letter + ") =" + tmp_text_original;
                        $p_result.text(p_result_text);
                        $p_result2.text("La factorisation a échouée.");
                        displayMath(p_result_text, document.getElementById('p-result'));
                    } else {
                        p_result_text = "P_(A)(" + unknown_var_letter + ") =" + tmp_text_original + " = " + tmp_text_factorized;
                        displayMath(p_result_text, document.getElementById('p-result'));
                    }
                } else if ($step == 3) {
                    var matrix = response.matrix;

                    if (matrix.type != "complex") {
                        var matrix_characteristic_polynomial_data = matrix.characteristic_polynomial.data;

                        var result_polynomial = matrix_characteristic_polynomial_data.coefficients;
                        var result_roots = matrix_characteristic_polynomial_data.roots;
                        var result_mul = matrix_characteristic_polynomial_data.multiplicities;

                        var tmp_text_original = canonicalPolynomialToString(result_polynomial, unknown_var_letter);

                        if (result_roots.length > 0) {
                            var tmp_text_factorized = factorizedPolynomialToString(matrix_characteristic_polynomial_data, unknown_var_letter);
                            displayMath("P_(A)(" + unknown_var_letter + ") =" + tmp_text_original + " = " + tmp_text_factorized, document.getElementById('p-result'));

                            //SPECTRE + VALEURS PROPRES
                            var tmp_spectre = "S p_(RR)(A) = {";
                            var first = true;
                            var tmp_mul = "";
                            var i = 0;
                            result_roots.forEach(function(str) {
                                if (!first) {
                                    tmp_spectre += ", ";
                                    tmp_mul += ", ";
                                } else {
                                    first = false;
                                }
                                tmp_spectre += str;
                                tmp_mul += "m(" + str + ") = " + result_mul[i];
                                i++;
                            });
                            tmp_spectre += "}";
                            displayMath(tmp_spectre + " avec " + tmp_mul, document.getElementById('p-result2'));
                        } else {
                            displayMath("P_(A)(" + unknown_var_letter + ") =" + tmp_text_original, document.getElementById('p-result'));
                            $p_result2.text("La factorisation a échouée.");
                        }
                    } else {
                        $p_result.text(p_result_text);
                    }
                }
            }
        });
    }

});

// Select all text in HTML input text when clicked
$(document).on("click", "input[type=text]", function(e) {
    this.select();
});

$( document ).ready(function(){
    $("#select-degree").val(3);
    $("#select-input-type").val('integer');
    $("#select-output-type").val('integer');
    changeDegree(3);
    $("#div-matrix input").attr('disabled','disabled');
});
