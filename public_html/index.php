<?php

define('DEBUG_FLAG', true);
define('PROJECT_BASE_DIR', __DIR__ . '/..');
define('ASSET_BASE_URL', '');

if (DEBUG_FLAG) {
    error_reporting(E_ALL);
}

function debug($var)
{
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

function dd($var)
{
    debug($var);
    die();
}

if (php_sapi_name() === 'cli-server') {
    // useless for now
} else {
    // useless for now
}

require_once PROJECT_BASE_DIR . '/vendor/autoload.php';
require_once PROJECT_BASE_DIR . '/app/routes.php';
