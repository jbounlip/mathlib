<?php
namespace Math;


class VectorTest extends \PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
    }

    protected function tearDown()
    {
    }

    public function testInvoke()
    {
        $v = new Vector([5, 7, 11]);

        $this->assertEquals(5, $v(0));
        $this->assertEquals(7, $v(1));
        $this->assertEquals(11, $v(2));
        $this->assertTrue(is_null($v(42)), 'access outbound index should return NULL');

        $v = new Vector([13, 17, 19], false);

        $this->assertEquals(13, $v(0));
        $this->assertEquals(17, $v(1));
        $this->assertEquals(19, $v(2));
        $this->assertTrue(is_null($v(42)), 'access outbound index should return NULL');
    }

    public function testEqualsTo()
    {
        $u = new Vector([1, 2, -3], true);

        $this->assertTrue($u->equalsTo([
            [1],
            [2],
            [-3],
        ]), 'it should be equals (columns array)');

        $v = new Vector([1, 2, -3], false);

        $this->assertTrue($v->equalsTo([
            [1, 2, -3],
        ]), 'it should be equals (rows array)');

        $w = new Vector([10, 20, -3], true);

        $this->assertFalse($u->equalsTo($w), 'it should not be equals (u != w)');

        $this->assertFalse($u->equalsTo($v), 'it should not be equals (u != v)');

        $this->assertFalse($v->equalsTo($u), 'it should not be equals (v != u)');
    }

    public function testEuclideanNorm()
    {
        $v = new Vector([5, 7, 11]);

        $this->assertEquals(sqrt(195), $v->euclideanNorm());

        $v = new Vector([5, 7, 11], false);

        $this->assertEquals(sqrt(195), $v->euclideanNorm());
    }

    public function testVectorScalarMultiply()
    {
        $c = -2.5;
        $v = new Vector([1, 2, -3], true);

        $ret = Vector::scalarMultiply($c, $v);

        $this->assertInstanceOf("Math\\Vector", $ret);
        $this->assertTrue($ret->equalsTo($exp = new Vector(
            [-2.5, -5, 7.5], true
        )), "wrong result (0)\n- actual:\n$ret- expected:\n$exp");

        $v = new Vector([1, 2, -3], false);

        $ret = Vector::scalarMultiply($c, $v);

        $this->assertInstanceOf("Math\\Vector", $ret);
        $this->assertTrue($ret->equalsTo($exp = new Vector(
            [-2.5, -5, 7.5], false
        )), "wrong result (1)\n- actual:\n$ret- expected:\n$exp");
    }
}
