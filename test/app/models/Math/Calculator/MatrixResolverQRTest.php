<?php
namespace Math\Calculator;

use Math\Matrix;
use Math\Polynomial;

class MatrixResolverQRTest extends \PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
    }

    protected function tearDown()
    {
    }

    public function testResolve()
    {
        $A = new Matrix([
            [8, -10, 2],
            [-9, 6, 5],
            [-3, 8, -2],
        ]);

        $resolver = new MatrixResolverQR($A);

        $resolver->calculate();

        $ret = $resolver->getEigenvalues();

        // Warning: values could not be sorted
        $this->assertEquals($ret[0], 17.349816090066184, "eigen value (0)", 0.001);
        $this->assertEquals($ret[1], -6.957057945615456, "eigen value (1)", 0.001);
        $this->assertEquals($ret[2], 1.6072418555492725, "eigen value (2)", 0.001);
    }

    public function testResolve2()
    {
        $A = new Matrix([
            [4, 4],
            [8, 4],
        ]);

        $resolver = new MatrixResolverQR($A);

        $resolver->calculate();

        $ret = $resolver->getEigenvalues();

        // Warning: values could not be sorted
        $this->assertEquals($ret[0], 9.65685424949238, "eigen value (0)", 0.001);
        $this->assertEquals($ret[1], -1.6568542494923806, "eigen value (1)", 0.001);
    }

    public function testResolve3()
    {
        $A = new Matrix([
            [-1, 2, 0],
            [2, 2, -3],
            [-2, 2, 1],
        ]);

        $resolver = new MatrixResolverQR($A);

        $resolver->calculate();

        $ret = $resolver->getEigenvalues();

        // Warning: values could not be sorted
        $this->assertEquals($ret[0], 1, "eigen value (0)", 0.001);
        $this->assertEquals($ret[1], 1, "eigen value (1)", 0.001);
        $this->assertEquals($ret[2], 0, "eigen value (2)", 0.001);
    }

    public function testResolve4()
    {
        $A = new Matrix([
            [8, -10, 2, 45, 100],
            [-9, 6, 5, 5, 100],
            [-3, 8, 2, 10, 100],
            [-3, 15, -6, 10, 100],
            [1, 2, 3, 4, 5],
        ]);

        $resolver = new MatrixResolverQR($A);

        $resolver->calculate();

        $ret = $resolver->getEigenvalues();

        // Warning: values could not be sorted
        $this->assertEquals($ret[0], 38.807, "eigen value (0)", 0.001);
        $this->assertEquals($ret[1], -19.257, "eigen value (1)", 0.001);
        $this->assertEquals($ret[2], 14.698, "eigen value (2)", 0.001);
        $this->assertEquals($ret[3], -9.956, "eigen value (3)", 0.001);
        $this->assertEquals($ret[4], 6.707, "eigen value (4)", 0.001);
    }

    public function testResolveWithComplexEigenvalues_Example1()
    {
        $A = new Matrix([
            [8, -10, 2, 4],
            [-9, 6, 5, 5],
            [1, 2, -3, 10],
            [4, 5, -6, 8],
        ]);

        $resolver = new MatrixResolverQR($A);

        $resolver->calculate();

        $ret = $resolver->getEigenvalues();

        // Warning: values could not be sorted
        $this->assertEquals($ret[0], 16.611, "eigen value (0)", 0.001);
        $this->assertEquals($ret[1], 8.443, "eigen value (1)", 0.001);
        $this->assertTrue($ret[2]->equalsTo([-3.027, 3.643]), "eigen value (2) - complex (actual: $ret[2])");
        $this->assertTrue($ret[3]->equalsTo([-3.027, -3.643]), "eigen value (3) - complex (actual: $ret[3])");
    }

    public function testResolveWithComplexEigenvalues_Example2()
    {
        // $p = new Polynomial([1364,121,17,5,1]);
        // $p->companionMatrix();
        $A = new Matrix([
            [0, 0, 0, -1364],
            [1, 0, 0, -121],
            [0, 1, 0, -17],
            [0, 0, 1, -5],
        ]);

        $resolver = new MatrixResolverQR($A);

        $resolver->calculate();

        $ret = $resolver->getEigenvalues();

        // Warning: values could not be sorted
        $this->assertTrue($ret[0]->equalsTo([-5.23623, 3.71476]), "eigen value (3) - complex (actual: $ret[0])");
        $this->assertTrue($ret[1]->equalsTo([-5.23623, -3.71476]), "eigen value (3) - complex (actual: $ret[1])");
        $this->assertTrue($ret[2]->equalsTo([2.73623, 5.06021]), "eigen value (2) - complex (actual: $ret[2])");
        $this->assertTrue($ret[3]->equalsTo([2.73623, -5.06021]), "eigen value (2) - complex (actual: $ret[3])");

    }
}
