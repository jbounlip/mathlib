<?php
namespace Math\Calculator;

use Math\Matrix;

class MatrixDecompositionQRHouseholderTest extends \PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
    }

    protected function tearDown()
    {
    }

    public function testProcess()
    {
        $A = new Matrix([
            [12, -51, 4],
            [6, 167, -68],
            [-4, 24, -41]
        ]);

        $decomposer = new MatrixDecompositionQRHouseholder($A);

        $decomposer->calculate();
        $ret = $decomposer->getQ();

        $this->assertInstanceOf("Math\\Matrix", $ret);
        // Warning: it can be equals to -expected_Q
        $this->assertTrue($ret->equalsTo($exp = new Matrix(array(
            [-6/7, 69/175, 58/175],
            [-3/7, -158/175, -6/175],
            [2/7, -6/35, 33/35],
        ))), "wrong result (Q)\n- actual:\n$ret- expected:\n$exp");

        $ret = $decomposer->getR();

        $this->assertInstanceOf("Math\\Matrix", $ret);
        // Warning: it can be equals to -expected_R
        $this->assertTrue($ret->equalsTo($exp = new Matrix(array(
            [-14, -21, 14],
            [0, -175, 70],
            [0, 0, -35],
        ))), "wrong result (R)\n- actual:\n$ret- expected:\n$exp");

        $ret = Matrix::multiply($decomposer->getQ(), $decomposer->getR());

        $this->assertInstanceOf("Math\\Matrix", $ret);
        $this->assertTrue($ret->equalsTo($exp = $A), "wrong result (A=Q.R)\n- actual:\n$ret- expected:\n$exp");

        $ret = Matrix::multiply($decomposer->getQ(), Matrix::transpose($decomposer->getQ()));

        $this->assertInstanceOf("Math\\Matrix", $ret);
        $this->assertTrue($ret->equalsTo($exp = Matrix::identity(3)), "wrong result (Q.QT=I)\n- actual:\n$ret- expected:\n$exp");
    }

    public function testProcess2()
    {
        $A = new Matrix([
            [8, -10, 2],
            [-9, 6, 5],
            [-3, 8, -2],
        ]);

        $decomposer = new MatrixDecompositionQRHouseholder($A);

        $decomposer->calculate();
        $ret = $decomposer->getQ();

        $this->assertInstanceOf("Math\\Matrix", $ret);
        // Warning: signs can change, most important is A=QR with Q orthogonal and R triangular
        $this->assertTrue($ret->equalsTo($exp = new Matrix(array(
            [-0.645, -0.291, 0.707],
            [0.725, -0.525, 0.445],
            [0.242, 0.800, 0.550],
        ))), "wrong result (Q)\n- actual:\n$ret- expected:\n$exp");

        $ret = $decomposer->getR();

        $this->assertInstanceOf("Math\\Matrix", $ret);
        // Warning: signs can change, most important is A=QR with Q orthogonal and R triangular
        $this->assertTrue($ret->equalsTo($exp = new Matrix(array(
            [-12.410, 12.732, 1.853],
            [0.000, 6.156, -4.808],
            [0.000, 0.000,  2.539],
        ))), "wrong result (R)\n- actual:\n$ret- expected:\n$exp");

        $ret = Matrix::multiply($decomposer->getQ(), $decomposer->getR());

        $this->assertInstanceOf("Math\\Matrix", $ret);
        $this->assertTrue($ret->equalsTo($exp = $A), "wrong result (A=Q.R)\n- actual:\n$ret- expected:\n$exp");

        $ret = Matrix::multiply($decomposer->getQ(), Matrix::transpose($decomposer->getQ()));

        $this->assertInstanceOf("Math\\Matrix", $ret);
        $this->assertTrue($ret->equalsTo($exp = Matrix::identity(3)), "wrong result (Q.QT=I)\n- actual:\n$ret- expected:\n$exp");
    }

    public function testProcess3()
    {
        $A = new Matrix([
            [-1, 9, 2, 8, 7],
            [5, 6, -5, 7, 2],
            [-9, 0, 1, 2, -3],
        ]);

        $decomposer = new MatrixDecompositionQRHouseholder($A);

        $decomposer->calculate();
        $ret = $decomposer->getQ();

        $this->assertInstanceOf("Math\\Matrix", $ret);
        // Warning: signs can change, most important is A=QR with Q orthogonal and R triangular
        $this->assertTrue($ret->equalsTo($exp = new Matrix(array(
            [-0.09667, -0.86558, -0.49136],
            [0.48337, -0.47237, 0.73703],
            [-0.87006, -0.16625, 0.46406],
        ))), "wrong result (Q)\n- actual:\n$ret- expected:\n$exp");

        $ret = $decomposer->getR();

        $this->assertInstanceOf("Math\\Matrix", $ret);
        // Warning: signs can change, most important is A=QR with Q orthogonal and R triangular
        $this->assertTrue($ret->equalsTo($exp = new Matrix(array(
            [10.34408, 2.03015, -3.48025, 0.87006, 2.90021],
            [0, -10.62443, 0.46446, -10.56373, -6.50502],
            [0, 0, -4.20382, 2.15651, -3.35760],
        ))), "wrong result (R)\n- actual:\n$ret- expected:\n$exp");

        $ret = Matrix::multiply($decomposer->getQ(), $decomposer->getR());

        $this->assertInstanceOf("Math\\Matrix", $ret);
        $this->assertTrue($ret->equalsTo($exp = $A), "wrong result (A=Q.R)\n- actual:\n$ret- expected:\n$exp");

        $ret = Matrix::multiply($decomposer->getQ(), Matrix::transpose($decomposer->getQ()));

        $this->assertInstanceOf("Math\\Matrix", $ret);
        $this->assertTrue($ret->equalsTo($exp = Matrix::identity(3)), "wrong result (Q.QT=I)\n- actual:\n$ret- expected:\n$exp");
    }
}
