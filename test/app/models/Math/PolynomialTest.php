<?php

namespace Math;


class PolynomialTest extends \PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
    }

    protected function tearDown()
    {
    }

    public function testConstruct()
    {
        $p = new Polynomial(array(1, 2, -3, 4));

        $ret = $p->toArray(false);

        $exp = array(1, 2, -3, 4);
        $this->assertEquals($exp, $ret, "wrong result\n- actual:\n" . print_r($ret, true) ."\n- expected:\n" . print_r($exp, true));

        $p = new Polynomial(array(
            'roots' => array(-5, 3),
            'multiplicities' => array(2, 1),
            'scalar_factor' => -1,
        ), true);

        $ret = $p->toArray(true);

        $exp = array(
            'roots' => array(-5, 3),
            'multiplicities' => array(2, 1),
            'scalar_factor' => -1,
        );
        $this->assertEquals($exp, $ret, "wrong result (1)\n- actual:\n" . print_r($ret, true) ."\n- expected:\n" . print_r($exp, true));

        $ret = $p->toArray(false);

        $exp = array(75, 5, -7, -1);
        $this->assertEquals($exp, $ret, "wrong result (2)\n- actual:\n" . print_r($ret, true) ."\n- expected:\n" . print_r($exp, true));
    }

    public function testDegree()
    {
        $p = new Polynomial(array(1, 2, -3, 4));

        $ret = $p->getDegree();

        $this->assertEquals(3, $ret);

        $p = new Polynomial(array(1, 2, -3, 4, 0, 0));

        $ret = $p->getDegree();

        $this->assertEquals(3, $ret);

        $p = new Polynomial(array(
            'roots' => array(-5, 3),
            'multiplicities' => array(2, 1),
            'scalar_factor' => -1,
        ), true);

        $ret = $p->getDegree();

        $this->assertEquals(3, $ret);
    }

    public function testIsValid()
    {
        $p = new Polynomial(array(1, 2, -3));

        $this->assertTrue($p->isValid(), 'Polynomial should be valid');

//        $p = new Polynomial(array('a'));
//
//        $this->assertFalse($p->isValid(), 'Polynomial should not be valid');

        $p = new Polynomial(array(
            'roots' => array(-5, 3),
            'multiplicities' => array(2, 1),
            'scalar_factor' => -1,
        ), true);

        $this->assertTrue($p->isValid(), 'Polynomial should be valid (1)');

        $p = new Polynomial(array(
            'roots' => array(-5, 3),
            'multiplicities' => array(2, 1, 1),
            'scalar_factor' => -1,
        ), true);

        $this->assertFalse($p->isValid(), 'Polynomial should not be valid (1)');
    }

    public function testEqualsTo()
    {
        $p = new Polynomial(array(1, 2, -3));
        $q = new Polynomial(array(1, 2, -3));

        $this->assertTrue($p->equalsTo($q), 'it should be equal');

        $p = new Polynomial(array(1, 2, -3));
        $q = array(1, 2, -3);

        $this->assertTrue($p->equalsTo($q), 'it should be equal (1)');

        $p = new Polynomial(array(1, 2, -3));
        $q = new Polynomial(array(1, 2, -30));

        $this->assertFalse($p->equalsTo($q), 'it should not be equal');

        $p = new Polynomial(array(1, -3, 3, -1));
        $q = new Polynomial([
            'roots' => [1],
            'multiplicities' => [3],
            'scalar_factor' => -1
        ], true);

        $this->assertTrue($p->equalsTo($q), 'it should be equal (2)');
    }

    public function testEqualsZero()
    {
        $p = new Polynomial([0]);

        $this->assertTrue($p->equalsZero(), 'it should be equal to zero');

        $p = new Polynomial([0, 1]);

        $this->assertFalse($p->equalsZero(), 'it should not be equal to zero');

        $p = new Polynomial([1]);

        $this->assertFalse($p->equalsZero(), 'it should not be equal to zero (1)');

    }

    public function testCalculateForValue()
    {
        $p = new Polynomial(array(1, 2, -3, 4));

        $ret = $p->calculateForValue(3);

        $this->assertEquals(1 + 2 * 3 + (-3) * 9 + 4 * 27, $ret);
    }

    public function testCalculateForValue_WithDegree5()
    {
        $p = new Polynomial(array(1, 2, -3, 4, -1, 2));

        $ret = $p->calculateForValue(3);

        $this->assertEquals(1 + 2 * pow(3, 1) + (-3) * pow(3, 2) + 4 * pow(3, 3) + (-1) * pow(3, 4) + 2 * pow(3, 5), $ret);
    }

    public function testCalculateForValue_WithComplexInputType()
    {
        $this->markTestIncomplete();
    }

    public function testCalculateIntegerRoots_WithExample1()
    {
        $p = new Polynomial(array(4, -8, 5, -1));

        $p->calculateIntegerRoots(-10, 10);
        $ret = $p->getRoots();

        $this->assertTrue(is_array($ret), 'return value should be an array');
        $this->assertTrue(in_array(1, $ret), '1 should be in array');
    }

    public function testCalculateIntegerRoots_WithExample2()
    {
        $p = new Polynomial(array(6, -11, 6, -1));

        $p->calculateIntegerRoots(-10, 10);
        $ret = $p->getRoots();

        $this->assertTrue(is_array($ret), 'return value should be an array');
        $this->assertTrue(in_array(1, $ret), '1 should be in array');
        $this->assertTrue(in_array(2, $ret), '2 should be in array');
        $this->assertTrue(in_array(3, $ret), '3 should be in array');
    }

    public function testCalculateIntegerRoots_WithExample3()
    {
        $p = new Polynomial(array(-16, 12, 0, -1));

        $p->calculateIntegerRoots(-10, 10);
        $ret = $p->getRoots();

        $this->assertTrue(is_array($ret), 'return value should be an array');
        $this->assertTrue(in_array(-4, $ret), '-4 should be in array');
        $this->assertTrue(in_array(2, $ret), '2 should be in array');
    }

    public function testCalculateIntegerRoots_WithExample4()
    {
        $p = new Polynomial(array(1, -3, 3, -1));

        $p->calculateIntegerRoots(-10, 10);
        $ret = $p->getRoots();

        $this->assertTrue(is_array($ret), 'return value should be an array');
        $this->assertTrue(in_array(1, $ret), '1 should be in array');
    }

    public function testCalculateMultiplicities_WithExample1()
    {
        $p = new Polynomial(array(6, -11, 6, -1));

        $p->calculateIntegerRoots(-10, 10);
        $p->calculateMultiplicities();

        $this->assertEquals(1, $p->getRoot(0), '1 should be a root');
        $this->assertEquals(2, $p->getRoot(1), '2 should be a root');
        $this->assertEquals(3, $p->getRoot(2), '3 should be a root');
        $this->assertEquals(1, $p->getMultiplicity(0), '1 should be the multiplicity of 1');
        $this->assertEquals(1, $p->getMultiplicity(1), '1 should be the multiplicity of 2');
        $this->assertEquals(1, $p->getMultiplicity(2), '1 should be the multiplicity of 3');
    }

    public function testCalculateMultiplicities_WithExample2()
    {
        $p = new Polynomial(array(-16, 12, 0, -1));

        $p->calculateIntegerRoots(-10, 10);
        $p->calculateMultiplicities();

        $this->assertEquals(-4, $p->getRoot(0), '-4 should be a root');
        $this->assertEquals(2, $p->getRoot(1), '2 should be a root');
        $this->assertEquals(1, $p->getMultiplicity(0), '1 should be the multiplicity of -4');
        $this->assertEquals(2, $p->getMultiplicity(1), '2 should be the multiplicity of 2');
    }

    public function testCalculateMultiplicities_WithExample3()
    {
        $p = new Polynomial(array(-1, 3, -3, 1));

        $p->calculateIntegerRoots(-10, 10);
        $p->calculateMultiplicities();

        $this->assertEquals(1, $p->getRoot(0), '1 should be a root');
        $this->assertEquals(3, $p->getMultiplicity(0), '3 should be the multiplicity of 1');
    }

    public function testRecalculateIntegerRootsAndMultiplicities_WithExample4()
    {
        // P(x) = x^3 - x^2 - 21^x + 45 = (x - 3)^2(x + 5)
        $p = new Polynomial(array(45, -21, -1, 1));

        $p->calculateIntegerRoots(-10, 10);
        $p->calculateMultiplicities();

        $this->assertEquals(-5, $p->getRoot(0), '-5 should be a root');
        $this->assertEquals(3, $p->getRoot(1), '1 should be a root');
        $this->assertEquals(1, $p->getMultiplicity(0), '1 should be the multiplicity of -5');
        $this->assertEquals(2, $p->getMultiplicity(1), '2 should be the multiplicity of 3');
    }

    public function testRecalculateIntegerRootsAndMultiplicities_WithExample5()
    {
        // P(x) = -x^3 - 7x^2 + 5^x + 75 = -(x - 3)(x + 5)^2
        $p = new Polynomial(array(75, 5, -7, -1));

        $p->calculateIntegerRoots(-10, 10);
        $p->calculateMultiplicities();

        $this->assertEquals(-5, $p->getRoot(0), '-5 should be a root');
        $this->assertEquals(3, $p->getRoot(1), '1 should be a root');
        $this->assertEquals(2, $p->getMultiplicity(0), '2 should be the multiplicity of -5');
        $this->assertEquals(1, $p->getMultiplicity(1), '1 should be the multiplicity of 3');
    }

    public function testFactorize_WithIntegerAlgorithmAndExample2()
    {
        $p = new Polynomial(array(-16, 12, 0, -1), false, 'integer', 'integer');

        $p->factorize(array(
            'try_from' => -10,
            'try_to' => 10,
        ));
        $ret = $p->toArray(true);

        $exp = array(
            'roots' => array(-4, 2),
            'multiplicities' => array(1, 2),
            'scalar_factor' => -1
        );
        $this->assertEquals($exp, $ret, "wrong result\n- actual:\n" . print_r($ret, true) ."\n- expected:\n" . print_r($exp, true), 0.000001);
    }

    public function testExpandAndSimplify_WithExample2()
    {
        $p = new Polynomial(array(
            'roots' => array(-4, 2),
            'multiplicities' => array(1, 2),
            'scalar_factor' => -1
        ), true);

        $p->expandAndSimplify();

        $ret = $p->toArray(false);

        $exp = array(-16, 12, 0, -1);
        $this->assertEquals($exp, $ret, "wrong result\n- actual:\n" . print_r($ret, true) ."\n- expected:\n" . print_r($exp, true), 0.000001);
    }

    public function testCompanionMatrix()
    {
        $p = new \Math\Polynomial(array(-16, 12, 0, -1), false, 'integer', 'integer');

        $ret = $p->companionMatrix();

        $this->assertInstanceOf("\\Math\\Matrix", $ret);
        $exp = array(
            [0, 0, -16],
            [1, 0, 12],
            [0, 1, 0],
        );
        $this->assertEquals($exp, $ret->toArray(), "wrong result\n- actual:\n" . print_r($ret->toArray(), true) ."\n- expected:\n" . print_r($exp, true), 0.000001);
    }

    public function testAdd()
    {
        $p = new Polynomial(array(-75, -5, 7, 1));
        $q = new Polynomial(array(70, 8, -7));

        $ret = $p->operatorAdd($q)->toArray();

        $exp = array(-5, 3, 0, 1);
        $this->assertEquals($exp, $ret, "wrong result\n- actual:\n" . print_r($ret, true) ."\n- expected:\n" . print_r($exp, true), 0.000001);

        $p = new Polynomial(array(-75, -5, 7, 1));
        $q = new Polynomial(array(1, 0, 0, 0));

        $ret = $p->operatorAdd($q)->toArray();

        $exp = array(-74, -5, 7, 1);
        $this->assertEquals($exp, $ret, "wrong result\n- actual:\n" . print_r($ret, true) ."\n- expected:\n" . print_r($exp, true), 0.000001);
    }

    public function testSubtract()
    {
        $p = new Polynomial(array(-75, -5, 7, 1));
        $q = new Polynomial(array(70, 8, -7));

        $ret = $p->operatorSubtract($q)->toArray();

        $exp = array(-145, -13, 14, 1);
        $this->assertEquals($exp, $ret, "wrong result\n- actual:\n" . print_r($ret, true) ."\n- expected:\n" . print_r($exp, true), 0.000001);
    }

    public function testScalarMultiply()
    {
        $p = new Polynomial(array(-75, -5, 7, 1));
        $k = -3.1;

        $ret = $p->operatorScalarMultiply($k)->toArray();

        $exp = array(232.5, 15.5, -21.7, -3.1);
        $this->assertEquals($exp, $ret, "wrong result\n- actual:\n" . print_r($ret, true) ."\n- expected:\n" . print_r($exp, true), 0.000001);
    }

    public function testMultiply()
    {
        $this->markTestSkipped();
    }

    public function testEuclideanDivide()
    {
        $p = new Polynomial(array(0, -2, 3, -1, -1, 1)); // P(x) = x^5 - x^4 - x^3 + 3x^2 - 2x
        $q = new Polynomial(array(1, -1, 1)); // Q(x) = x^2 - x + 1

        $ret = $p->operatorEuclideanDivide($q);
        // QQ(x) = x^3 - 2x + 1
        // R(x) = x - 1

        $this->assertCount(2, $ret, "it should return array of polynomials (0)");
        $this->assertArrayHasKey('quotient', $ret, "it should return array of polynomials (1)");
        $this->assertArrayHasKey('remainder', $ret, "it should return array of polynomials (2)");
        $this->assertInstanceOf('\\Math\\Polynomial', $ret['quotient'], "it should return array of polynomials (3)");
        $this->assertInstanceOf('\\Math\\Polynomial', $ret['remainder'], "it should return array of polynomials (4)");
        $this->assertEquals($exp = [1, -2, 0, 1], $ret['quotient']->toArray(), "wrong result\n- actual:\n" . print_r($ret['quotient']->toArray(), true) ."\n- expected:\n" . print_r($exp, true), 0.000001);
        $this->assertEquals($exp = [-1, 1], $ret['remainder']->toArray(), "wrong result (1)\n- actual:\n" . print_r($ret['remainder']->toArray(), true) ."\n- expected:\n" . print_r($exp, true), 0.000001);
    }

    public function testEuclideanDivide_Example2()
    {
        $p = new Polynomial(array(0, 0, -8, 0, 4, 4)); // P(x) = 4x^5 - 4x^4 - 8x^2
        $q = new Polynomial(array(0, -2, 0, 1, 1)); // Q(x) = x^4 + 3x^3 - 2x

        $ret = $p->operatorEuclideanDivide($q);
        // QQ(x) = 4x
        // R(x) = 0

        $this->assertCount(2, $ret, "it should return array of polynomials (0)");
        $this->assertArrayHasKey('quotient', $ret, "it should return array of polynomials (1)");
        $this->assertArrayHasKey('remainder', $ret, "it should return array of polynomials (2)");
        $this->assertInstanceOf('\\Math\\Polynomial', $ret['quotient'], "it should return array of polynomials (3)");
        $this->assertInstanceOf('\\Math\\Polynomial', $ret['remainder'], "it should return array of polynomials (4)");
        $this->assertEquals($exp = [0, 4], $ret['quotient']->toArray(), "wrong result\n- actual:\n" . print_r($ret['quotient']->toArray(), true) ."\n- expected:\n" . print_r($exp, true), 0.000001);
        $this->assertEquals(true, $ret['remainder']->equalsZero(), "wrong result (1)\n- actual:\n" . print_r($ret['remainder']->toArray(), true) ."\n- expected:\n" . print_r($exp, true), 0.000001);
    }

    public function testEuclideanDivide_Example3()
    {
        $p = new Polynomial(array(75, 5, -7, -1)); // P(x) = -(x - 3)(x + 5)^2
        $q = new Polynomial(array(25, 10, 1)); // Q(x) = (x + 5)^2

        $ret = $p->operatorEuclideanDivide($q);
        // QQ(x) = -x + 3
        // R(x) = 0

        $this->assertCount(2, $ret, "it should return array of polynomials (0)");
        $this->assertArrayHasKey('quotient', $ret, "it should return array of polynomials (1)");
        $this->assertArrayHasKey('remainder', $ret, "it should return array of polynomials (2)");
        $this->assertInstanceOf('\\Math\\Polynomial', $ret['quotient'], "it should return array of polynomials (3)");
        $this->assertInstanceOf('\\Math\\Polynomial', $ret['remainder'], "it should return array of polynomials (4)");
        $this->assertEquals($exp = [3, -1], $ret['quotient']->toArray(), "wrong result\n- actual:\n" . print_r($ret['quotient']->toArray(), true) ."\n- expected:\n" . print_r($exp, true), 0.000001);
        $this->assertEquals(true, $ret['remainder']->equalsZero(), "wrong result (1)\n- actual:\n" . print_r($ret['remainder']->toArray(), true) ."\n- expected:\n" . print_r($exp, true), 0.000001);
    }
}
