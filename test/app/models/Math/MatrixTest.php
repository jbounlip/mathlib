<?php
namespace Math;


class MatrixTest extends \PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
    }

    protected function tearDown()
    {
    }

    public function testInvoke()
    {
        $m = new Matrix([
            [1, 2, 3],
            [4, 5, 6],
            [1, 0, 0],
        ]);

        $this->assertEquals(1, $m(0,0));
        $this->assertEquals(2, $m(0,1));
        $this->assertEquals(3, $m(0,2));
        $this->assertEquals(4, $m(1,0));
        $this->assertEquals(5, $m(1,1));
        $this->assertEquals(6, $m(1,2));
        $this->assertEquals(1, $m(2,0));
        $this->assertEquals(0, $m(2,1));
        $this->assertEquals(0, $m(2,2));
        $this->assertTrue(is_null($m(42,0)), 'access outbound index should return Null');
    }

    public function testRowVectorArray()
    {
        $m = new Matrix([
            [1, 2, 3],
            [4, 5, 6],
            [1, 0, 0],
        ]);

        $ret = $m->rowVectorArray(0);

        $this->assertTrue(is_array($ret), 'it should be an array (1)');
        $this->assertEquals(1, $ret[0]);
        $this->assertEquals(2, $ret[1]);
        $this->assertEquals(3, $ret[2]);

        $ret = $m->rowVectorArray(2);

        $this->assertTrue(is_array($ret), 'it should be an array (2)');
        $this->assertEquals(1, $ret[0]);
        $this->assertEquals(0, $ret[1]);
        $this->assertEquals(0, $ret[2]);
    }

    public function testColumnVectorArray()
    {
        $m = new Matrix([
            [1, 2, 3],
            [4, 5, 6],
            [1, 0, 0],
        ]);

        $ret = $m->columnVectorArray(0);

        $this->assertTrue(is_array($ret), 'it should be an array (1)');
        $this->assertEquals(1, $ret[0]);
        $this->assertEquals(4, $ret[1]);
        $this->assertEquals(1, $ret[2]);
    }

    public function testIsValid()
    {
        $m = new Matrix([
            [1, 2, 3, 1],
            [4, 5, 6, -2],
            [1, 0, 0, 4],
        ]);
        $this->assertTrue($m->isValid(), 'it should be valid');

        $m = new Matrix([
            [],
        ]);
        $this->assertFalse($m->isValid(), 'it should not be valid');

        $m = new Matrix([
        ]);
        $this->assertFalse($m->isValid(), 'it should not be valid');

        $m = new Matrix([
            [1, 2, 3],
            [4, 5, 6],
            [1, 0],
        ]);
        $this->assertFalse($m->isValid(), 'it should not be valid');

        $m = new Matrix([
            [1, 2],
            [4, 'a'],
            [1, 0],
        ]);
        $this->assertFalse($m->isValid(), 'it should not be valid');
    }

    public function testEqualsTo()
    {
        $a = new Matrix([
            [1, 2, 3],
            [1, 0, 0],
        ]);
        $b = new Matrix([
            [1, 2, 3],
            [1, 0, 0],
        ]);
        $this->assertTrue($a->equalsTo($b), 'it should be equal');

        $a = new Matrix([
            [1, 2, 3],
            [1, 0, 0],
        ]);
        $b = array(
            [1, 2, 3],
            [1, 0, 0],
        );
        $this->assertTrue($a->equalsTo($b), 'it should be equal');

        $a = new Matrix([
            [1, 2, 3],
            [1, 0, 0],
        ]);
        $b = new Matrix([
            [1, 2, 3],
            [1, 0, 1],
        ]);
        $this->assertFalse($a->equalsTo($b), 'it should not be equal');

    }

    public function testIsSquare()
    {
        $m = new Matrix([
            [1, 2, 3],
            [4, 5, 6],
            [1, 0, 0],
        ]);
        $this->assertTrue($m->isSquare(), 'it should be a square matrix');

        $m = new Matrix([
            [1, 2],
            [4, 5],
            [1, 0],
        ]);
        $this->assertFalse($m->isSquare(), 'it should not be a square matrix');
    }

    public function testTrace()
    {
        $m = new Matrix([
            [1, 2, 3],
            [4, 5, 6],
            [1, 0, 0],
        ]);
        $this->assertEquals(6, $m->trace());

        $m = new Matrix([
            [1, 2],
            [4, 5],
            [1, 0],
        ]);
        $this->assertTrue(is_null($m->trace()), 'it should return Null');
    }

    public function testMatrixAdd()
    {
        $a = new Matrix([
            [1, 2, 3],
            [1, 0, 0],
        ]);
        $b = new Matrix([
            [-1, 2, 10],
            [1, 0, -5],
        ]);

        $ret = Matrix::add($a, $b);

        $this->assertInstanceOf("Math\\Matrix", $ret);
        $this->assertTrue($ret->equalsTo($exp = new Matrix(array(
            [0, 4, 13],
            [2, 0, -5],
        ))), "wrong result\n- actual:\n$ret- expected:\n$exp");

        $c = new Matrix([
            [-1, 2],
            [1, 0],
        ]);

        $ret = Matrix::add($a, $c);

        $this->assertTrue(is_null($ret), 'wrong sum should return null');
    }

    public function testMatrixTranspose()
    {
        $m = new Matrix([
            [1, 2, 3],
            [1, 0, 0],
        ]);

        $ret = Matrix::transpose($m);

        $this->assertInstanceOf("Math\\Matrix", $ret);
        $this->assertTrue($ret->equalsTo($exp = new Matrix(array(
            [1, 1],
            [2, 0],
            [3, 0],
        ))), "wrong result\n- actual:\n$ret- expected:\n$exp");
    }

    public function testMatrixMultiply()
    {
        $a = new Matrix([
            [1, 2, 3],
            [1, 0, 0],
        ]);
        $b = new Matrix([
            [-1, 2, 1, 2],
            [1, 0, 3, 1],
            [2, 0, 0, 1],
        ]);

        $ret = Matrix::multiply($a, $b);

        $this->assertInstanceOf("Math\\Matrix", $ret);
        $this->assertTrue($ret->equalsTo($exp = new Matrix(array(
            [7, 2, 7, 7],
            [-1, 2, 1, 2],
        ))), "wrong result\n- actual:\n$ret- expected:\n$exp");
    }

    public function testMatrixScalarMultiply()
    {
        $c = -2.5;
        $a = new Matrix([
            [1, 2, -3],
            [1, 0, 0],
        ]);

        $ret = Matrix::scalarMultiply($c, $a);

        $this->assertInstanceOf("Math\\Matrix", $ret);
        $this->assertTrue($ret->equalsTo($exp = new Matrix(array(
            [-2.5, -5, 7.5],
            [-2.5, 0, 0],
        ))), "wrong result\n- actual:\n$ret- expected:\n$exp");
    }

    public function testCharacteristicPolynomial_ForDegree3_WithExample1()
    {
        $m = new Matrix([
            [-1, 2, 0],
            [2, 2, -3],
            [-2, 2, 1],
        ]);

        $ret = $m->characteristicPolynomial();

        $this->assertInstanceOf("Math\\Polynomial", $ret);
        $this->assertTrue($ret->equalsTo($exp = new Polynomial(array(
            0, -1, 2, -1
        ))), "wrong result\n- actual:\n$ret\n- expected:\n$exp");
    }

    public function testCharacteristicPolynomial_ForDegree3_WithExample2()
    {
        $m = new Matrix([
            [3, 4, -1],
            [-1, 1, 1],
            [0, 3, 2],
        ]);

        $ret = $m->characteristicPolynomial();

        $this->assertInstanceOf("Math\\Polynomial", $ret);
        $this->assertTrue($ret->equalsTo($exp = new Polynomial(array(
            8, -12, 6, -1
        ))), "wrong result\n- actual:\n$ret\n- expected:\n$exp");
    }

    public function testCharacteristicPolynomial_ForDegree4()
    {
        $m = new Matrix([
            [-3, 0, -1, 0],
            [4/5, 0, 1, 2],
            [-1/7, 0, -5, 0],
            [0, 1, 0, 0],
        ]);

        $ret = $m->characteristicPolynomial();

        $this->assertInstanceOf("Math\\Polynomial", $ret);
        $exp = new Polynomial(array(
            -29.7142857, -16, 12.8571429, 8, 1
        ));
        $this->assertTrue($exp->equalsTo($ret, false, 0.000001), "wrong result\n- actual:\n$ret\n- expected:\n$exp");
    }

    public function testEigenvalues_ForDegree4()
    {
        $m = new Matrix([
            [-3, 0, -1, 0],
            [4/5, 0, 1, 2],
            [-1/7, 0, -5, 0],
            [0, 1, 0, 0],
        ]);

        $ret = $m->eigenvalues();
        $exp = array(
            -5.06904496, -2.93095503, 1.4142135, -1.4142135,
        );
        $this->assertEquals($exp, $ret, "wrong result\n- actual:\n" . print_r($ret, true) ."\n- expected:\n" . print_r($exp, true), 0.000001);
    }

} 