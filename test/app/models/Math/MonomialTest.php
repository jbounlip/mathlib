<?php

namespace Math;


class MonomialTest extends \PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
    }

    protected function tearDown()
    {
    }

    public function testConstruct()
    {
        $p = new Monomial(42, 5);

        $ret = $p->toArray(false);

        $exp = array(0, 0, 0, 0, 0, 42);
        $this->assertEquals($exp, $ret, "wrong result\n- actual:\n" . print_r($ret, true) ."\n- expected:\n" . print_r($exp, true), 0.000001);
    }

    public function testDivide()
    {
        $p = new Monomial(42, 5);
        $q = new Monomial(5, 4);

        $ret = $p->operatorDivide($q)->toArray();

        $exp = array(0, 8.4);
        $this->assertEquals($exp, $ret, "wrong result\n- actual:\n" . print_r($ret, true) ."\n- expected:\n" . print_r($exp, true), 0.000001);
    }

    public function testPolynomialMultiply()
    {
        $p = new Monomial(4, 1); // P(x) = 4x
        $q = new Polynomial([0, -2, 0, 1, 1]); // Q(x) = x^4 + x^3 - 2x

        $ret = $p->operatorPolynomialMultiply($q)->toArray();

        $exp = array(0, 0, -8, 0, 4, 4);
        $this->assertEquals($exp, $ret, "wrong result\n- actual:\n" . print_r($ret, true) ."\n- expected:\n" . print_r($exp, true), 0.000001);
    }
} 