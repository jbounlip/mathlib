<?php
namespace Controllers;

class MathControllerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var MathController
     */
    protected $object;

    protected function setUp()
    {
        $this->object = new MathController();
    }

    protected function tearDown()
    {
    }

    public function testGetPolynomialRoots()
    {
        $request_args = [
            'json' => '{"polynomial":[6,-11,6,-1],"input_type":"integer","output_type":"integer"}'
        ];

        $ret = $this->object->getPolynomialRoots($request_args);

        $this->assertEquals('{"polynomial":{"type":"partial","data":{"input_type":"integer","output_type":"integer","roots":[1,2,3]}}}', $ret);
    }

    public function testGetPolynomialFactorization()
    {
        $request_args = [
            'json' => '{"polynomial":[-16,12,0,-1],"input_type":"integer","output_type":"integer"}'
        ];

        $ret = $this->object->getPolynomialFactorization($request_args);

        $this->assertEquals('{"polynomial":{"type":"factorized","data":{"input_type":"integer","output_type":"integer","roots":[-4,2],"multiplicities":[1,2],"scalar_factor":-1}}}', $ret);
    }

    public function testGetMatrixCharacteristicPolynomial()
    {
        $request_args = [
            'json' => '{"matrix":[[-1, 2, 0],[2, 2, -3],[-2, 2, 1]],"input_type":"integer","output_type":"integer"}'
        ];

        $ret = $this->object->getMatrixCharacteristicPolynomial($request_args);

        $this->assertEquals('{"matrix":{"characteristic_polynomial":{"type":"full","data":{"input_type":"integer","output_type":"integer","coefficients":[0,-1,2,-1],"roots":[0,1],"multiplicities":[1,2],"scalar_factor":-1}}}}', $ret);
    }

    public function testGetMatrixCharacteristicPolynomial_CodomainReal()
    {
        $json_string = '{"matrix":[["1","3","-15","-1","12"],["-7","-4","2","13","12"],["-5","3","-8","6","12"],["-3","-7","12","4","11"],["13","10","13","0","-4"]],"input_type":"integer","output_type":"float","try_from":-10,"try_to":10,"precision":"0.000001","max_iteration":"100","float_precision":"3"}';
        $request_args = [
            'json' => $json_string
        ];

        $ret = $this->object->getMatrixCharacteristicPolynomial($request_args);
        var_dump($ret);

        $this->assertNotEquals('{"matrix":{"characteristic_polynomial":{"type":"full","data":{"input_type":"integer","output_type":"float","coefficients":[-229792,5803,1531,472,-11,-1],"roots":[-27.07,18.29],"multiplicities":[1,1],"scalar_factor":-1}}}}', $ret);
        $this->assertEquals('{"matrix":{"characteristic_polynomial":{"type":"full","data":{"input_type":"integer","output_type":"float","coefficients":[-229792,5803,1531,472,-11,-1],"roots":[],"multiplicities":[],"scalar_factor":null}}}}', $ret);
    }
}
